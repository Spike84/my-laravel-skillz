<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
           'role-list',
           'role-create',
           'role-edit',
           'role-delete',
           'user-list',
           'user-edit',
           'user-delete',
           'product-list',
           'product-show',
           'product-create',
           'product-edit',
           'product-delete',
           'region-list',
           'region-create',
           'region-edit',
           'region-delete',
           'user-show',
           'page-list',
           'page-create',
           'page-edit',
           'page-delete',
           'cat-list',
           'cat-create',
           'cat-edit',
           'cat-delete',
           'prop-list',
           'prop-create',
           'prop-edit',
           'prop-delete',
           'tproduct-list',
           'tproduct-create',
           'tproduct-edit',
           'tproduct-delete',
           'importlog-list',
           'importlog-create',
           'importlog-edit',
           'importlog-show',
           'importlog-delete',
           'order-list',
           'order-create',
           'order-edit',
           'order-show',
           'order-delete',
           'img-list',
           'img-create',
           'img-delete',
           'mailing-list',
           'mailing-create',
           'change-settings',
           'access-admin'
        ];

		$exist_perm = Permission::all('name');
		foreach($exist_perm as $item) 
		{
			$permissions['exists'][] = $item->name;
		};

        foreach ($permissions as $key=>$val)
        {
             if ($key !='exists' && !in_array($val,$permissions['exists'])) {
             	Permission::create(['name' => $val]);
             }
        };
    }
}
