<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mailusers', function (Blueprint $table) {
            $table->id();
            $table->string('fio', 255)->nullable();
            $table->string('email', 255);
            $table->text('url')->nullable();
            $table->foreignId('user_id')
				->constrained()
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->foreignId('mailing_id')
				->constrained()
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->tinyInteger('is_sent')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mailusers');
    }
}
