<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('importlogs', function (Blueprint $table) {
            $table->id();
            $table->biginteger('user_id');
            $table->biginteger('importtype_id');
            $table->biginteger('importstage_id');
            $table->tinyInteger('has_errors')->default(0);
            $table->biginteger('cat_id');
            $table->text('result')->nullable();
            $table->timestamps('started_at')->nullable();
            $table->timestamps('finished_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('importlogs');
    }
}
