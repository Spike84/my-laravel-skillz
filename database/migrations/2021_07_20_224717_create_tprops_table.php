<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTpropsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tprops', function (Blueprint $table) {
            $table->id();
            $table->integer('sort')->default(0);
            $table->string('name', 255)->nullable();
            $table->string('sku', 255)->nullable();
            $table->string('product_sku', 255)->nullable();
            $table->string('value_sku', 255)->nullable();
            $table->string('value', 255)->nullable();
            $table->foreignId('importlog_id')
				->constrained()
				->onUpdate('cascade')
				->onDelete('cascade');
            $table->timestamps();
        });

        /*Schema::table('tprops', function (Blueprint $table) {
			
		});*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tprops');
    }
}
