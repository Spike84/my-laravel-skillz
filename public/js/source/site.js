$(document).ready(function() {

	$(function() {
		var windowCurSize = $(window).width();
		if(windowCurSize < 850) {
			$('html, body').stop().animate({
		        scrollTop: $('#monitorScrollContent').offset().top - 20
		    }, 200);
		}
	});

	$(".basket_quantity .basket_minus").click(function() {
		var num = $(this).next().val();
		if(num>0) {
			num = num-1;
		}
		$(this).next().val(num);
		$(this).next().attr('value', num);
		//$(this).parent(".basket_quantity").submit();
	});

	$(".basket_quantity .basket_plus").click(function() {
		var num = $(this).prev().val();
		num = Number(num) + 1;
		$(this).prev().val(num);
		$(this).prev().attr('value', num);
		//$(this).parent(".basket_quantity").submit();
	});

	$(window).scroll(function(){
		if($(this).scrollTop() != 0){
			$('#toTop').fadeIn();
		}else{
			$('#toTop').fadeOut();
		}
	});

	$('#toTop').click(function(){
		$('body,html').animate({scrollTop:0},800);
	});


	//filters submit on click
	var FilterFormSubmit = function() {
        $('#filter_form').submit();
    }

	$('#filter_form input:checkbox').on('click', FilterFormSubmit);
	$('#filter_form select').on('change', FilterFormSubmit);

    /*$('#filter_form select option:selected').click(function() {
		if(!$(this).is(':animated') {
        	$('#filter_form').submit();
    	}
    });*/

	$('.ajaxBasket_add').submit(function(event) {
		var product_id = $(this).children('.ajaxBasket_productID').attr('value');
		var quantity = $(this).children().children().children('.ajaxBasket_quantity').attr('value');
		$('.ajaxBasket_add_'+product_id).fadeOut(400);

		$.ajax('/lk/baskets/ajax_add', {
			type: 'POST',
			dataType: 'json',
			data: { 
				product_id: product_id,
				quantity: quantity
			},
			 headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    },
			success: function() {
				$('.ajaxBasket_add_'+product_id).remove();
				$('.ajaxBasket_Addsuccess_'+product_id).fadeIn(1000);
				var basketNum = $('#ajaxBasket_NumProds').text();
				basketNum = +basketNum + 1;
				$('#ajaxBasket_NumProds').empty().append(basketNum);
			},
			error: function() {
				$('.ajaxBasket_add_'+product_id).fadeIn(1000);
				$('.ajaxBasket_error_'+product_id).remove();
				$('.ajaxBasket_add_'+product_id).append('<p class="text-center ajaxBasket_error_'+product_id+'"><span class="text-danger">Ошибка</span> добавления в корзину</p>');
			}
		});

		event.preventDefault();
	});

});
