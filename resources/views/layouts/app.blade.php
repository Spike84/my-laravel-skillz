<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="@yield('description')"> 

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('/css/compiled.css') }}">

        <!-- Scripts -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script src="{{ asset('/js/source/site.js') }}"></script>
    </head>
    <body>
    	<!-- Yandex.Metrika counter -->
		<script type="text/javascript" >
		   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
		   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
		   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

		   ym(27430415, "init", {
		        clickmap:true,
		        trackLinks:true,
		        accurateTrackBounce:true,
		        webvisor:true
		   });
		</script>
		<noscript><div><img src="https://mc.yandex.ru/watch/27430415" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->

        <div class="wrapper" id="app">
            <header>
        		<div class="container">
        		<div class="row row-cols-sm-1 row-cols-md-2 header panel">
        			<div class="col menu_panel">
                		@include('layouts.elements.menu')
        			</div>
        			<div class="col logo_panel">
        				<a href="/"><img src="/img/logo.png" class="logo"></a>
        			</div>
        		</div>
        		</div>
                @include('layouts.elements.auth')
            </header>
            
            <!-- Page Content -->
             <!--
             @if(View::hasSection('title'))
                <h1>@yield('title')</h1>
             @endif
             -->
             <div class="container">
				<div class="row row-cols-sm-1 row-cols-md-2">
					<div class="col-md-3 mb-4">
						@include('layouts.elements.filter')
					</div>
					<div class="col-md-9">
						@include('layouts.elements.mess')
						<a name="content" id="monitorScrollContent"></a>
                        @yield('content')
                    </div>
                  </div>
             </div>
             
             <p>&nbsp;</p>
             
             <div id="app">
            	<example-component></example-component>
            </div>

             
            <div class="empty">&nbsp;</div>
        </div>
        <footer id="footer" class="footer navbar-fixed-bottom">
            <div class="container">
                <div class="text-center">ООО "Торговый дом Фавор" @php echo date('Y'); @endphp. Создано в <a href="http://progphp.ru" target="_blank">ProgPHP</a>.</div>
            </div>
        </footer>
        <div id="toTop" class="toTop">&nbsp;</div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
