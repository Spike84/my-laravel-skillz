<div class="pe-4">
	<form method="get" id="filter_form" action="{{ route('products') }}">

		<div class="row row-cols-sm-1 row-cols-md-2 mb-4">
			<div class="col">
				Категория
			</div>
			<div class="col">
				@if(isset($cats))
					@foreach($cats as $cat)
						<div><a href="/products?cat_id={{ $cat->id }}" @if (isset($input['cat_id']) && $input['cat_id'] == $cat->id) class="text-muted text-decoration-none" @endif>{{ $cat->name }}</a></div>
						@if (isset($input['cat_id']) && $input['cat_id'] == $cat->id)
							<input name="cat_id" type="hidden" value="{{ $cat->id }}" />
						@endif
					@endforeach
				@endif
			</div>
		</div>

		<div class="row row-cols-md-1 mb-4">
			<div class="col-4">
				Наличие
			</div>
			<div class="col-8">
				<select name="in_stock" class="form-group w-100">
					<option value="2" @if(isset($input['in_stock']) && $input['in_stock'] == 2) selected @endif>в наличии и под заказ</option>
					<option value="1" @if(isset($input['in_stock']) && $input['in_stock'] == 1) selected @endif>только в наличии</option>
				</select>
			</div>
		</div>
		@if(isset($propvals) && !empty($propvals))
			@foreach($propvals as $propval_key => $propval_val)
				@if(count($propval_val) > 1)
				<div class="row mb-4">
					<div class="col-4 text-end">
						{{ $propval_key }}
					</div>
					<div class="col-8">
						@foreach($propval_val as $propval)
							<nobr><input name="prop[{{ $propval->prop_id }}][]" type="checkbox" value="{{ $propval->propvalue_id }}" @if(isset($input['prop'][$propval->prop_id]) && in_array($propval->propvalue_id, $input['prop'][$propval->prop_id])) checked @endif />{{ $propval->value }}</nobr>
						@endforeach
					</div>
				</div>
				@endif
			@endforeach
		@endif

		<div class="row mb-4">
			<div class="">
				Название
			</div>
			<div class="">
				<input type="text" placeholder="Поиск..." name="searchw" class="form-group w-100" value="{{ $input['searchw'] ?? '' }}" />
			</div>
		</div>

		<div class="row mb-4 text-center">
			<div class="col"><a href="{{ route('products') }}">Отмена</a></div>
			<div class="col"><button class="base_button" type="submit">Поиск</button></div>
		</div>

	</form>
</div>