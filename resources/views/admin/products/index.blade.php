@extends('layouts.admin')

@section('content')

<h1>Все товары</h1>

<div class="container">

	<h2>Товары</h2>

	<p>&nbsp;</p>

	<div class="row">
		<div class="col text-end">
			<form method="get" action="{{ route('admin.products.index') }}">
				<div class="">
					 <input type="text" placeholder="Поиск..." name="searchw" class="form-group w-100" value="{{ $input['searchw'] ?? '' }}" />
				</div>
				<div class="mb-2 text-muted small">поиск по названию, артикулу, SKU</div>
				<div class="mb-2">
					<button class="" type="submit">Применить</button>
				</div>
		    </form>
    	</div>
	</div>
	
	<div class="row">
		<div class="col">
			{!! $products->appends(Request::except('page'))->render() !!}
		</div>
		<div class="col text-end">
			Всего: {{ $products->total() }}
		</div>
	</div>

	<div class="row">
		<div class="col table-responsive">
		    <table class="table table-striped table-hover table-sm table-bordered">
		        <thead>
		        <tr class="table-secondary text-center">
		            <th>#</th>
					<th rowspan="2">@sortablelink('sort', 'Сорт')</th>
					<th rowspan="2"></th>
		            <th>@sortablelink('name', 'Название')</th>
		            <th rowspan="2">@sortablelink('articul', 'Артикул')</th>
					<th rowspan="2">@sortablelink('weight', 'Вес')</th>
					<th>@sortablelink('price', 'Цена')</th>
		            <th rowspan="2" title="Активность на сайте">ОП</th>
		            <th rowspan="2"></th>
		        </tr>
				<tr class="table-secondary text-center">
		            <th>(ID)</th>
		            <th>(Категория)</th>
					<th>@sortablelink('quantity', '(Кол-во)')</th>
		        </tr>
		        </thead>
		        <tbody>
		            @php
		                $i = 0;
		            @endphp
		            @foreach ($products as $product)
		                @php
		                    $i++;
		                @endphp
		                <tr>
		                    <td class="text-center">{{ $i }} ({{ $product->id }})</td>
		                    <td class="text-center">{{ $product->sort }}</td>
		                    <td style="max-width: 100px;">
		                    	<div style="width: 100px;">
		                    	@if($product->imgs->count() > 0)
		                    		<a data-fancybox="gallery" href="{{ Storage::disk('public')->url($product->imgs[0]->url."/".$product->imgs[0]->file) }}"><img src="{{ Helper::image_resizer($product->imgs[0]->url."/".$product->imgs[0]->file, 100) }}" class="w-100" /></a>
		                    	@else
		                    		<img src="{{ Storage::disk('public')->url('catalog/noimg.jpg') }}" alt="" class="w-100" />
			                    @endif
			                    </div>
			                </td>
		                    <td>
		                    	@can('product-show')<a href="{{ route('admin.products.show', $product->id) }}">@endcan
		                    	{{ $product->name }}
		                    	@can('product-show')</a>@endcan
		                    	@if($product->children->count() > 0) (+{{ $product->children->count() }}) @endif
		                    	({{ $product->cat->name ?? '' }})
			                </td>
		                    <td class="text-center">{{ $product->articul }}</td>
		                    <td class="text-center">{{ $product->weight }}</td>
		                    <td class="text-center">{{ $product->price }} ({{ $product->quantity }})
		                    </td>
		                    <td class="text-center">
		                        @if ($product->is_active == 1)
		                            <span class="text-success">Да</span>
		                        @else
		                            <span class="text-danger">Нет</span>
		                        @endif
		                    </td>
		                    <td class="text-center actions">
		                    	@can('product-edit')
		                    	<a class="act_butt" title="Редактировать" href="{{ route('admin.products.edit', $product->id) }}">ред.</a>
		                    	@endcan

		                    	@can('product-delete')
		                    	@if($product->children->count() == 0)
		                        <form action="{{ route('admin.products.destroy', ['product' => $product->id]) }}"
			                          method="post" onsubmit="return confirm('Вы действительно хотите удалить {{ $product->name }} ?')">
			                          @csrf
			                          @method('DELETE')
			                          <button type="submit" title="Удалить" class="act_butt m-0 border-0 bg-transparent text-primary"><u>X</u></button>
			                    </form>
			                    @endif
			                    @endcan
		                    </td>
		                </tr>
		            @endforeach
		        </tbody>
		    </table>

		</div>
	</div>

	<div class="row">
		<div class="col">
			{!! $products->appends(Request::except('page'))->render() !!}
		</div>
		<div class="col text-end">
			Всего: {{ $products->total() }}
		</div>
	</div>

</div>

@endsection
