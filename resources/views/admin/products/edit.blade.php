@extends('layouts.admin')

@section('content')
	<div class="admin-form">
		<h2><span>Редактирование товара "{{ $product->articul }}"</span></h2>

		<form method="post" action="{{ route('admin.products.update', $product->id) }}">
	        @method('PATCH')
		    @csrf
	        
	        <div class="container">
	        	<div class="row row-cols-1 row-cols-lg-2">

					<div class="col">
				    	<div class="container">

				    		<div class="row mb-2">
								<div class="col-4"><label for="is_active">Активен</label></div>
								<div class="col-8">
				            		<input class="form-group" type="checkbox" name="is_active" @if ($product->is_active == 1) checked @endif>
								</div>
				        	</div>

							<div class="row mb-2">
							    <div class="col-4"><label for="sort">Сортировка</label></div>
							    <div class="col-8">
							        <input type="text" name="sort" class="form-group w-100" value="{{ old('sort') ?? $product->sort ?? '' }}" required />
							    </div>
							</div>

							<div class="row mb-2">
							    <div class="col-4"><label for="articul">Артикул</label></div>
							    <div class="col-8">
							        <input type="text" name="articul" class="form-group w-100" value="{{ old('articul') ?? $product->articul ?? '' }}" required />
							    </div>
							</div>

							<div class="row mb-2">
							    <div class="col-4"><label for="name">Категория</label></div>
							    <div class="col-8">
							        <select name="cat_id" class="form-group">
								    	<option>-- не выбрано --</option>
									    @foreach($cats as $cat)
							                <option value="{{ $cat->id }}" @if ($product->cat_id == $cat->id || old('cat_id') == $cat->id) selected @endif>{{ $cat->name }}</option>
							            @endforeach
						            </select>
							    </div>
							</div>

					    </div>
				    </div>

		        	<div class="col">
		        		<div class="container">

							<div class="row mb-2">
							    <div class="col-4"><label for="weight">Вес (г)</label></div>
							    <div class="col-8">
							        <input type="text" name="weight" class="form-group w-100" value="{{ old('weight') ?? $product->weight ?? '' }}" />
							    </div>
							</div>

		        			<div class="row mb-2">
							    <div class="col-4"><label for="quantity">Количество</label></div>
							    <div class="col-8">
							        <input type="text" name="quantity" class="form-group w-100" value="{{ old('quantity') ?? $product->quantity ?? '' }}" />
							    </div>
							</div>

							<div class="row mb-2">
							    <div class="col-4"><label for="price">Цена</label></div>
							    <div class="col-8">
							        <input type="text" name="price" class="form-group w-100" value="{{ old('price') ?? $product->price ?? '' }}" />
							    </div>
							</div>

		        		</div>
				    </div>

			    </div>
		    </div>

			<div class="container">
		    	<div class="row">
		    		<div class="col mt-2">
		    			Название
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col">
		    			<input type="text" name="name" class="form-group w-100" value="{{ old('name') ?? $product->name ?? '' }}" />
		    		</div>
		    	</div>
		    	<div class="row mt-2">
		    		<div class="col">
		    			SKU
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col">
		    			<input type="text" name="sku" class="form-group w-100" value="{{ old('sku') ?? $product->sku ?? '' }}" />
		    		</div>
		    	</div>
		    	<div class="row mt-2">
		    		<div class="col">
		    			Комментарий
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col">
		    			<textarea name="comment" class="form-control" rows="2">{{ old('comment') ?? $product->comment ?? '' }}</textarea>
		    		</div>
		    	</div>
		    </div>

			<div class="container buttons_block">
	        	<div class="row text-center justify-content-center">
	        		<div class="buttons_block_sub" style="width: 400px;">

	        			<div class="container">
	        				<div class="row">
								<div class="col">
						            <button class="" type="submit">Сохранить</button>
						        </div>
						        <div class="col"><a href="{{ route('admin.products.index') }}">Отмена</a></div>
						    </div>
						</div>

			    	</div>
				</div>
	        </div>
	        
	    </form>
    </div>
@endsection
