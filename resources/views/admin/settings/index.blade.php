@extends('layouts.admin')

@section('content')

<script type="text/javascript">
	$(document).ready(function() {
		$('.admin_settings_list .form-check-input').click(function() {
			var el = '#' + $(this).attr('id');

			$(el).fadeOut(300);

			var id = $(this).attr('data-id');
			var is_active = $(this).attr('value');
			if(is_active == 0) {
				var new_is_active = 1;
			} else {
				var new_is_active = 0;
			}
			var url = '/admin/settings/edit/' + id + '/' + is_active;

			$.ajax(url, {
				type: 'GET',
				success: function() {
					$(el).fadeIn(300);
					$(el).attr('value', new_is_active);
				},
				error: function() {
					$(el).parent().html('error. try again');
				}
			});
		});
	});
</script>

<div class="container admin_settings_list">

	<h2>Настройки</h2>

	<p>&nbsp;</p>

	@foreach($settings as $setting)
		<div class="row" style="max-width: 400px;">
			<div class="col">
				{{ $setting->name }}
			</div>
			<div class="col">
				<div class="form-check form-switch">
					<input class="form-check-input" type="checkbox" role="switch" id="SettingParam{{ $setting->id }}" data-id="{{ $setting->id }}" @if($setting->is_active == 1) checked value="0" @else value="1" @endif>
				</div>
			</div>
		</div>
	@endforeach

</div>
@endsection
