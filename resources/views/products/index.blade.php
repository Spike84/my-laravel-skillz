@extends('layouts.app')

@if(isset($cat) && !empty($cat))
	@section('title', 'Товары категории '.$cat->name ?? '')
	@section('description', 'Товары категории '.$cat->name ?? '')
@else
	@section('title', 'Каталог продукции')
	@section('description', 'Каталог продукции')
@endif

@section('content')


@if(isset($cat) && !empty($cat))
	<h1>{{ $cat->name ?? '' }}</h1>
@else
	<h1>Все товары</h1>
@endif

	<div class="sort_links text-end"><span class="sorting_text">Сортировать по:</span> @sortablelink('price', 'Цене') <span class="divider">|</span> @sortablelink('quantity', 'Наличию') <span class="divider">|</span> @sortablelink('name', 'Названию') <span class="divider">|</span> @sortablelink('weight', 'Весу')</div>

	<div class="prod_catalog row row-cols-1 row-cols-sm-2 row-cols-xl-3 align-content-stretch">
	@foreach ($products as $product)
		<div class="prod_item d-flex flex-column">
			<div class="prod_img text-center mb-auto">
				<a class="d-block align-items-center" href="@if(isset($product->parent)) {{ route('products.show', $product->parent_id) }} @else {{ route('products.show', $product->id) }} @endif" style="background-image: url('@if($product->imgs->count() > 0){{ Helper::image_resizer($product->imgs[0]->url."/".$product->imgs[0]->file, 200) }}@else{{ Storage::disk('public')->url('catalog/noimg.jpg') }}@endif');">&nbsp;
				</a>
			</div>

			<div class="prod_props">
				<div class="prod_name text-center">{{ $product->name }}</div>
				<div class="row">
					<div class="col text-secondary text-end">категория: </div>
					<div class="col">{{ $product->cat->name ?? '' }}</div>
				</div>
				@if(!empty($product->articul))
					<div class="row">
						<div class="col text-secondary text-end">артикул: </div>
						<div class="col">{{ $product->articul }}</div>
					</div>
				@endif
				@if($product->weight != '0.000')
					<div class="row">
						<div class="col text-secondary text-end">сред. вес: </div>
						<div class="col">{{ $product->weight }} г</div>
					</div>
				@endif
				{{--
				<div class="row">
					<div class="col text-secondary text-end">sku: </div>
					<div class="col">{{ $product->sku }}</div>
				</div>
				<div class="row">
					<div class="col text-secondary text-end">SKU Категории: </div>
					<div class="col">{{ $product->cat->sku ?? '' }}</div>
				</div>
				--}}
			</div>

			<div class="prod_price_box mt-auto">
				@if($product->children_count == 0)
					<div class="row prod_price">
						@include('layouts.elements.price',[
							'prod_quantity' => $product->quantity,
							'prod_price' => $product->price,
						])
					</div>
					<div class="row prod_basket">
						@include('layouts.elements.add2basket',['product_id' => $product->id])
					</div>
				@endif
				<div class="row prod_read_more">
					<div class="col text-center">
						@if(isset($product->parent))
							<a href="{{ route('products.show', $product->parent->id) }}">подробнее</a>
						@else
							<a href="{{ route('products.show', $product->id) }}">подробнее</a>
						@endif
					</div>
				</div>
			</div>
		</div>
	@endforeach
	</div>

	<p>&nbsp;</p>

	<div class="row">
		{!! $products->appends(Request::except('page'))->render() !!}
	</div>

@endsection