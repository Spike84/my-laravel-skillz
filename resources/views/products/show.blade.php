@extends('layouts.app')

@section('title', 'Каталог.'.$product->name)
@section('description', 'Каталог.'.$product->name)

@section('content')

<h1>{{ $product->name }}</h1>


	<div class="row row-cols-1 row-cols-lg-2">
	    <div class="col">
	    	<div class="row">
		    	@if($product->imgs->count() > 0)
					<a data-fancybox="gallery" href="{{ Storage::disk('public')->url($product->imgs[0]->url."/".$product->imgs[0]->file) }}"><img src="{{ Helper::image_resizer($product->imgs[0]->url."/".$product->imgs[0]->file, 400) }}" class="w-100" /></a>
				@else
					<div class="d-flex justify-content-center align-items-center"><img src="{{ Storage::disk('public')->url('catalog/noimg.jpg') }}" alt="" class="w-100" /></div>
				@endif
			</div>
			@if($product->imgs->count() > 1)
			<div class="row detail_small_imgs">
				@foreach($product->imgs as $img)
					<div class="col">
						<a data-fancybox="gallery" class="d-block" href="{{ Storage::disk('public')->url($img->url."/".$img->file) }}" style="background-image: url('{{ Storage::disk('public')->url($img->url."/".$img->file) }}');"></a>
					</div>
				@endforeach
			</div>
			@endif
	    </div>
	    <div class="col">

			@if(!empty($product->articul))
			<div class="row">
				<div class="col text-secondary text-end">Артикул:</div>
				<div class="col">{{ $product->articul }}</div>
			</div>
			@endif

			@if($product->weight != '0.000')
			<div class="row">
				<div class="col text-secondary text-end">Средний вес: </div>
				<div class="col">{{ $product->weight }} г</div>
			</div>
			@endif
			@if(empty($addprods))
				<p>&nbsp;</p>
				@include('layouts.elements.price',[
					'prod_quantity' => $product->quantity,
					'prod_price' => $product->price,
				])
				<p>&nbsp;</p>
				@include('layouts.elements.add2basket',['product_id' => $product->id])
				<p>&nbsp;</p>
			@endif
			
			@if($product->propvals->count() != 0)
				@foreach ($product->propvals as $propval)
					@if(!empty($propval->value))
					<div class="row">
						<div class="col text-secondary text-end">{{ $propval->prop->name }}:</div>
						<div class="col"> {{ $propval->value }}</div>
					</div>
					@endif
				@endforeach
			@endif
			
			@php
				//dump($addprods);
				//dd($props);
			@endphp
			
			@if(isset($addprods['add_props']))
				@foreach ($addprods['add_props'] as $prop_key => $prop_val)
					@if(!empty($prop_val))
					<div class="row">
						<div class="col text-secondary text-end">{{ $props[$prop_key]['name'] ?? '' }}:</div>
						<div class="col">{{ $prop_val }}</div>
					</div>
					@endif
				@endforeach
			@endif
		</div>
	</div>

	@if(isset($addprods) && !empty($addprods))

		@php
			//dump($addprods);
			$first_prop = array_key_first($addprods['props']);
			$last_prop = array_key_last($addprods['props'])
		@endphp

		<p>&nbsp;</p>
		<h2>Характеристики</h2>
		<p>&nbsp;</p>

		@if(!Auth::check())
			<div class="col text-center text-muted small">для отображения характеристик, цен и заказа необходимо <a href="{{ route('login') }}">авторизоваться</a></div>
		@else

		<table class="table table-striped table-hover table-sm table-bordered addit_products">
		<thead class="sticky_head">
			@if(count($addprods['props']) == 2)
				<tr>
					<th></th>
					<th colspan="{{ count($addprods['props'][$first_prop]) }}" class="text-center">{{ $props[$first_prop]['name'] }}</th>
				</tr>
				<tr>
					<th class="text-center">{{ $props[$last_prop]['name'] }}</th>
				@foreach ($addprods['props'][$first_prop] as $fkey => $fval)
					<th class="text-center">{{ $fkey }}</th>
				@endforeach
			@elseif(count($addprods['props']) == 1)
				<tr>
				<th></th>
				<th class="text-center">Артикул</th>
				<th class="text-center">{{ $props[$first_prop]['name'] }}</th>
				<th class="text-center">Сред. вес</th>
				<th class="text-center">Цена</th>
				<th class="text-center"></th>
				</tr>
			@else
				<tr>
				<th></th>
				<th class="text-center">Артикул</th>
				<th class="text-center">Характеристики</th>
				<th class="text-center">Сред. вес</th>
				<th class="text-center">Цена</th>
				<th class="text-center"></th>
				</tr>
			@endif
		</thead>
		<tbody>
			@if(count($addprods['props']) == 2)
				@php
					$i = 0;
				@endphp
				@foreach ($addprods['props'][$last_prop] as $lkey => $lval)
				<tr>
					<td class="text-center">{{ $lkey }}</td>
					@foreach ($addprods['props'][$first_prop] as $fkey => $fval)
						<td>
							<div class="add2basket_tabled"></div>
							<div class="row">
							@foreach($fval as $fprod_id=>$fprod_details)
								@if(isset($lval[$fprod_id]))
									<div class="col">
										<div class="mb-2 text-center">{{ $lval[$fprod_id]['articul'] }}</div>
										<div class="mb-2 text-center">
											@if($lval[$fprod_id]['weight'] != '0.000')
												ср. вес: {{ $lval[$fprod_id]['weight'] }} г
											@endif
										</div>
										<div class="mb-4 mt-2">
											<span title="{{ $lval[$fprod_id]['articul'] }}">
												@include('layouts.elements.price',[
													'prod_quantity' => $lval[$fprod_id]['quantity'],
													'prod_price' => $lval[$fprod_id]['price'],
												])
											</span>
										</div>
										<div class="mb-2">
											@include('layouts.elements.add2basket',['product_id' => $fprod_id])
										</div>
									</div>
								@endif
							@endforeach
							</div>
						</td>
					@endforeach
				</tr>
				@endforeach
			@elseif(count($addprods['props']) == 1)
				@foreach ($addprods['props'][$first_prop] as $fkey => $fval)
					<tr>
						<td class="text-center" style="max-width: 100px;">
							@if(isset($fval[array_key_first($fval)]['img']) && !empty($fval[array_key_first($fval)]['img']))
								<a class="zoom" href="{{ Storage::disk('public')->url($fval[array_key_first($fval)]['img']) }}"><img src="{{ Helper::image_resizer($fval[array_key_first($fval)]['img'], 400) }}" class="w-100" /></a>
							@else
								<div class="d-flex justify-content-center align-items-center"><img src="{{ Storage::disk('public')->url('catalog/noimg.jpg') }}" alt="" class="w-100" /></div>
							@endif
						</td>
						<td class="text-center">{{ $fval[array_key_first($fval)]['articul'] }}</td>
						<td class="text-center">{{ $fkey }}</td>
						<td class="text-center">
							@if($fval[array_key_first($fval)]['weight'] != '0.000')
								{{ $fval[array_key_first($fval)]['weight'] }} г
							@endif
						</td>
						<td class="text-center">
							@include('layouts.elements.price',[
								'prod_quantity' => $fval[array_key_first($fval)]['quantity'],
								'prod_price' => $fval[array_key_first($fval)]['price'],
							])
						</td>
						<td class="text-center">@include('layouts.elements.add2basket',['product_id' => array_key_first($fval)])</td>
					</tr>
				@endforeach
			@else
				@foreach ($addprods['prods'] as $prod_key => $prod_val)
				<tr>
					<td class="text-center" style="max-width: 100px;">
						@if(isset($prod_val['product']['img']) && !empty($prod_val['product']['img']))
							<a class="zoom" href="{{ Storage::disk('public')->url($prod_val['product']['img']) }}"><img src="{{ Helper::image_resizer($prod_val['product']['img'], 400) }}" class="w-100" /></a>
						@else
							<div class="d-flex justify-content-center align-items-center"><img src="{{ Storage::disk('public')->url('catalog/noimg.jpg') }}" alt="" class="w-100" /></div>
						@endif
					</td>
					<td>{{ $prod_val['product']['articul'] }}</td>
					<td class="text-center">
						@foreach($prod_val['props'] as $prod_val_key => $prod_val_val)
							@if(!isset($addprods['add_props'][$prod_val_key]))
								{{ $props[$prod_val_key]['name'] }}: {{ $prod_val_val }}<br />
							@endif
						@endforeach
					</td>
					<td>
						@if($prod_val['product']['weight'] != '0.000')
							{{ $prod_val['product']['weight'] }} г
						@endif
					</td>
					<td class="text-center">
						@include('layouts.elements.price',[
							'prod_quantity' => $prod_val['product']['quantity'],
							'prod_price' => $prod_val['product']['price'],
						])
					</td>
					<td class="text-center">@include('layouts.elements.add2basket',['product_id' => $prod_key])</td>
				</tr>
				@endforeach
			@endif
		</tbody>
		</table>
		@endif

	@endif

@endsection