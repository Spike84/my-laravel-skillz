<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class UserRegistered extends Notification
{
    use Queueable;
	private $params;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        	->subject('Регистрация нового пользователя на сайте')
            ->line('На сайте '.env('APP_URL', false).' зарегистрировался новый пользователь '.$this->params['name'])
            ->line(new HtmlString($this->params['user_data']))
            ->line('Необходимо активировать пользователя')
            ->action('Активировать пользователя', url('/users/activate/'.$this->params['id'].'/'.$this->params['act_code']))
            ->line('Данное уведомление отправлено почтовым роботом. Отвечать на него не нужно!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
