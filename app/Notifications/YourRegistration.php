<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class YourRegistration extends Notification
{
    use Queueable;
    private $params;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        	->subject('Регистрация нового пользователя ТД Фавор')
            ->line('Ваш емейл был указан при подаче заявки на регистрацию на сайте '.env('APP_URL', false))
        	->line('Указанный при регистрации Логин: '.$this->params['name'])
        	->line('Наш менеджер свяжется с Вами для уточнения деталей регистрации и активации. После активации Вашей учетной записи нашим менеджером вы сможете авторизоваться на сайте, чтобы смотреть цены и наличие товаров и делать заказы на сайте.')
        	->line('Если вы не регистрировались на данном сайте, возможно произошла ошибка. Просто проигнорируйте данное письмо.')
            ->line('Данное уведомление отправлено почтовым роботом. Отвечать на него не нужно!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
