<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class MassMailNotify extends Notification
{
    use Queueable;

    private $mailuser;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($mailuser)
    {
        $this->mailuser = $mailuser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        	->subject($this->mailuser->subject)
            ->line(new HtmlString($this->mailuser->message))
        	->line(new HtmlString("<br /><br />Вы получили данное письмо, т.к. подписаны на E-mail рассылку на сайте <a href='".env('APP_URL', false)."/mmail?mailing=".$this->mailuser->id."'>".env('APP_URL', false)."</a><br /> Если Вы не хотите больше получать E-mail расылки с нашего сайта, Вам нужно перейти по <a href='".env('APP_URL', false)."/mmail/lk/users/edit?mailing=".$this->mailuser->id."'>данной ссылке</a> в редактирование профиля, выбрать пункт 'не получать' и сохранить измененные данные. <br />Вы также можете связаться с нашими менеджерами для отписки от рассылки." ));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
