<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class YourOrder extends Notification
{
    use Queueable;
    private $params;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        	->subject('Новый заказ на сайте ТД Фавор')
            ->line('Вами был оформлен новый заказ на сайте '.env('APP_URL', false))
        	->line('Номер заказа: '.$this->params['id'])
        	->line(new HtmlString($this->params['products']))
        	->line(new HtmlString($this->params['info']))
        	->line('Наш менеджер свяжется с Вами для уточнения деталей.')
        	->line('Если вы не делали заказ, возможно произошла ошибка. Просто проигнорируйте данное письмо.')
            ->line('Данное уведомление отправлено почтовым роботом. Отвечать на него не нужно!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
