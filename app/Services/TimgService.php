<?php

namespace App\Services;

use App\Models\Timg;
use App\Models\Img;
use App\Services\ImgService;
use DB;
use Helper;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class TimgService
{
	protected $imgService;

	function __construct(ImgService $imgService)
    {
         $this->imgService = $imgService;
    }

	/**
	 * Syncs tproducts main photos data with products
	 * @param object $importlog
	 * @param string $num_transacts
	*/
	public function syncMainPhotos($importlog, $num_transacts)
	{
		$importlog->importstage_id = 2;
		$importlog->started_at = date('Y-m-d H:i:s');
		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Задача обновления фото из директории начата.";
		
		$file = Helper::import_file_inf($importlog->importtype->url, $importlog->importtype->file);

		$importlog->result .= $file->result;
		
		if ($file->ready == 0) {
			$importlog->importstage_id = 3;
			$importlog->finished_at = date('Y-m-d H:i:s');

			Helper::saveImportlogActs($importlog);

			echo redirect()->route('admin.importlogs.index')
				->with('error', $file->result);
		}

		Helper::saveImportlogActs($importlog);

		$timg_count = 0;

		DB::table('timgs')
			->leftJoin('products AS products', 'products.sku', '=', 'timgs.sku')
			->select('products.id AS product_id', 'products.cat_id', 'products.parent_id', 'timgs.id', 'timgs.url', 'timgs.file')
			->where([
		       	['timgs.importlog_id', '=', $importlog->id],
		       ])
		    ->whereNotNull('products.id')
			->chunkById($num_transacts, function($timgs) use (&$timg_count, $importlog) {
				$timg_count = $timg_count + count($timgs);

				foreach ($timgs as $timg) 
				{
					//DB::beginTransaction();

					$img_dest = 'catalog/products/'.$timg->cat_id.'/'.$timg->product_id;
					$img_dir = $timg->url.'/'.$timg->file;

					$this->checkTimgFileExists($importlog, $img_dir);

					$old_img = Img::where([
						['imgable_id', '=', $timg->product_id],
						['imgable_type', '=', 'App\Models\Product'],
						['is_main', '=', 1],
					])
					->first();

					if(!empty($old_img)) {
						$this->imgService->deleteImg($old_img, $importlog);
					}

				    $this->createNewTimgImg($importlog, $timg, $img_dest, $img_dir, 1, $timg->product_id, 'фото по основному товару');

					//замена фото родительского товара
					if ($timg->parent_id != 0) {
						$img_parent_dest = 'catalog/products/'.$timg->cat_id.'/'.$timg->parent_id;

						$old_parent_img = Img::where([
							['imgable_id', '=', $timg->parent_id],
							['imgable_type', '=', 'App\Models\Product'],
							['is_main', '=', 1],
						])
						->first();

						if(!empty($old_parent_img)) {
							$this->imgService->deleteImg($old_parent_img, $importlog);
						}

						$this->createNewTimgImg($importlog, $timg, $img_parent_dest, $img_dir, 1, $timg->parent_id, 'фото по привязанному товару');
					}
					//\\замена фото родительского товара

					//$this->deleteTimg($timg, $importlog);

					//DB::commit();
				};

				$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Обработано ".count($timgs)." из ".$timg_count." изображений";

				Helper::saveImportlogActs($importlog);
		}, 'timgs.id', 'id');

		$importlog->importstage_id = 5;
		$importlog->finished_at = date('Y-m-d H:i:s');
		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Задача обновления данных закончена.";

		$importlog->save();
	}

	/**
	 * Syncs tproducts additional photos data with products
	 * @param object $importlog
	 * @param string $num_transacts
	*/
	public function syncAdditPhotos($importlog, $num_transacts)
	{
		$importlog->importstage_id = 2;
		$importlog->started_at = date('Y-m-d H:i:s');
		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Задача синхронизации дополнительных фото начата.";

		Helper::saveImportlogActs($importlog);

		$prod_count = 0;
		
		DB::table('timgs')
			->leftJoin('products AS products', 'products.sku', '=', 'timgs.sku')
			->select('timgs.id','timgs.sku', 'timgs.url', 'timgs.file', 'products.cat_id', 'products.id as product_id')
			->where([
		       	['timgs.importlog_id', '=', $importlog->id],
		    	['timgs.sku', '!=', NULL],
			])
			->chunkById($num_transacts, function($timgs) use (&$timg_count, $importlog) {
				$timg_count = $timg_count + count($timgs);

				foreach ($timgs as $timg)
				{
					$img_dest = 'catalog/products/'.$timg->cat_id.'/'.$timg->product_id;
					$img_dir = $timg->url.'/'.$timg->file;

					$this->checkTimgFileExists($importlog, $img_dir);
				    $this->createNewTimgImg($importlog, $timg, $img_dest, $img_dir, 0, $timg->product_id, 'фото дополнительного товара');
				    $this->deleteAdditTimg($importlog, $timg);
				};

				$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Обработано ".count($timgs)." из ".$timg_count." изображений";
				Helper::saveImportlogActs($importlog); 
			}, 'timgs.id', 'id');

		$importlog->importstage_id = 5;
		$importlog->finished_at = date('Y-m-d H:i:s');
		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Задача синхронизации данных закончена.";

		Helper::saveImportlogActs($importlog);
	}

	/**
	 * Deletes timg data and addit file
	 * @param object $timg
	 * @param object or false $importlog
	*/
    public function deleteTimg($timg, $importlog)
    {
        $file = $timg->url.'/'.$timg->file;
        $stats = "Успешно удалено";

		try {
			DB::table('timgs')->where('id', '=', $timg->id)->delete();

			$del = Storage::disk('import')->delete($file);
			if(!$del) {
				$stats = '. Ошибка удаления файла '.$file;
			}

			return $stats;
		} catch(\Illuminate\Database\QueryException $ex) {
			if ($importlog) {
				$importlog->result .= "\n<br />[err] ".date('Y-m-d H:i:s'). " - Ошибка удаления файла импорта ".$file;
				$importlog->has_errors = 1;
				Helper::saveImportlogActs($importlog);
			}
			return false;
		}
    }

	/**
	 * Creates files and db data for imgs from timgs
	 * @param object $importlog
	 * @param string $img_dir
	*/
    private function checkTimgFileExists($importlog, $img_dir)
    {
    	if(!Storage::disk('import')->exists($img_dir)) {
			$importlog->result .= "\n<br />[err] ".date('Y-m-d H:i:s'). " - Файл не найден на диске ".$img_dir;
			$importlog->has_errors = 1;
			Helper::saveImportlogActs($importlog);
		}
    }

	/**
	 * Creates files and db data for imgs from timgs
	 * @param object $importlog
	 * @param object $timg
	 * @param string $img_dest
	 * @param string $img_dir
	 * @param string $is_main
	 * @param string $imgable_id
	 * @param string $title
	*/
    private function createNewTimgImg($importlog, $timg, $img_dest, $img_dir, $is_main, $imgable_id, $title)
    {
    	try {
	    	$new_file = Storage::putFile('/public/'.$img_dest.'/', new File(env('APP_STORAGE_ROOT').'/app/import/'.$img_dir));
	    	$new_filename = pathinfo($new_file);

	    	$data = array();
			$data['url'] = $img_dest;
			$data['file'] = $new_filename['basename'];
			$data['imgable_id'] = $imgable_id;
			$data['imgable_type'] = "App\Models\Product";
			$data['is_main'] = $is_main;

			$img = Img::firstOrCreate($data,$data);
		}  catch(\Exception $e) {
	       	$importlog->result .= "\n<br />[err] ".date('Y-m-d H:i:s'). " - Ошибка создания нового ".$title." ".$timg->file;
	       	$importlog->has_errors = 1;
			Helper::saveImportlogActs($importlog);
	    }
    }

	/**
	 * Deletes additional timg from db
	 * @param object $timg
	 * @param object $importlog
	*/
    private function deleteAdditTimg($importlog, $timg)
    {
    	try {
			DB::table('timgs')->where('id', '=', $timg->id)->delete();
		} catch(\Exception $e) {
			$importlog->result .= "\n<br />[err] ".date('Y-m-d H:i:s'). " - Ошибка удаления дополнительного изображения из базы импорта ".$timg->id;
			$importlog->has_errors = 1;
			Helper::saveImportlogActs($importlog);
		}
    }

}
