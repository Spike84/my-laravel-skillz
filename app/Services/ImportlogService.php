<?php

namespace App\Services;

use App\Models\Importlog;
use App\Models\Setting;
use Helper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;


class ImportlogService
{

	/**
	 * Creates new importlog
	 * @param object $importtype
	 * @param string $cat_id
	 * @param bool $auto
	*/
    public function createImportlog($importtype, $cat_id, $auto)
    {
        $file = Helper::import_file_inf($importtype->url, $importtype->file);

		if ($file->ready == 0) {
			$this->processActionResults($auto, 'error', $file->result);
			die();
		}

		$input['result'] = $file->result;
		$input['cat_id'] = $cat_id;
		$input['importtype_id'] = $importtype->id;

		if (!$auto) {
			$input['user_id'] = Auth::id();
		} else {
			$input['user_id'] = 0;
		}

		if (!$auto) {
			$input['importstage_id'] = 1;
		} else {
			$input['importstage_id'] = 2;
		}

		$input['result'] .= "\n<br />".date('Y-m-d H:i:s'). " - Создана задача загрузки данных.";

		try {
        	$importlog = Importlog::create($input);
        	$this->processActionResults($auto, 'status', 'Успешно создано');
        	
        	return $importlog;
        } catch(\Exception $e) {
        	$this->processActionResults($auto, 'error', 'Ошибка создания задачи');

			//here log/email error for auto
			//...
			die();
		}
    }

	/**
	 * Creates new importlog
	 * @param bool $auto
	 * @param string $type
	 * @param string $message
	*/
    public function processActionResults($auto, $type, $message)
    {
    	if (!$auto) {
			echo redirect()->route('admin.importlogs.index')
				->with($type, $message);

			die();
		}
    }

	/**
	 * Checks if setting param is active
	 * @param string $id
	*/
    public function checkSettingParam($id)
    {
    	$setting = Setting::find($id);

    	if ($setting && $setting->is_active == 1) {
    		return $setting->name;
    	}

    	return false;
    }

}
