<?php

namespace App\Services;

use App\Models\Cat;
use App\Models\Importlog;
use App\Models\Product;
use App\Models\Tproduct;
use App\Services\ImportlogService;
use App\Services\ProductService;
use Helper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;


class TproductService
{
	protected $productService;
	protected $importlogService;

    function __construct(ProductService $productService, ImportlogService $importlogService)
    {
         $this->productService = $productService;
         $this->importlogService = $importlogService;
    }

	/**
	 * Parces the .csv file and creates new items in tproducts
	 * @param object $importlog
	 * @param string $num_transacts
	 * @param bool $auto
	*/
    public function createTproducts($importlog, $num_transacts, $auto)
    {
        $importlog->importstage_id = 2;
		$importlog->started_at = date('Y-m-d H:i:s');
		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Задача загрузки данных из файла начата.";

		$file = Helper::import_file_inf($importlog->importtype->url, $importlog->importtype->file);

		$importlog->result .= $file->result;

		if ($file->ready == 0) {
			$importlog->importstage_id = 1;
			$importlog->finished_at = date('Y-m-d H:i:s');

			Helper::saveImportlogActs($importlog);

			$this->importlogService->processActionResults($auto, 'error', $file->result);
			die();
		}

		$parsed_params = array();

		if($importlog->importtype_id == 1) {
			$parsed_params['tab_names'][0] = "sku";
			$parsed_params['tab_names'][1] = "parent_sku";
			$parsed_params['tab_names'][2] = "parent_name";
			$parsed_params['tab_names'][3] = "articul";
			$parsed_params['tab_names'][4] = "name";
			$parsed_params['tab_names'][5] = "weight";
			$parsed_params['tab_names'][6] = "comment";
			$parsed_params['tab_names'][7] = "cat_sku";
			$parsed_params['tab_names'][8] = "cat_name";

			$parsed_params['num_col'] = 9;
			$parsed_params['col_replace_points'] = array(5);
			$parsed_params['col_replace_space'] = array(5);
			$parsed_params['col_sku'] = 0;
		} elseif($importlog->importtype_id == 2 || $importlog->importtype_id == 6) {
			$parsed_params['tab_names'][0] = "sku";
			$parsed_params['tab_names'][1] = "articul";
			$parsed_params['tab_names'][2] = "price";
			$parsed_params['tab_names'][3] = "quantity";
			$parsed_params['tab_names'][4] = "weight";

			$parsed_params['num_col'] = 5;
			$parsed_params['col_replace_points'] = array(2,3,4);
			$parsed_params['col_replace_space'] = array(2,3,4);
			$parsed_params['col_sku'] = 0;
		}

		unset($file->conts[0]);

		$i = 0;
		$collection = collect($file->conts)->chunk($num_transacts);

		foreach ($collection as $data) 
		{
			$i++;
			$save_data = Helper::csv_main_parser($importlog, $parsed_params, $data);

			try {
				Tproduct::insert($save_data);
				$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - этап ".$i." из ".count($collection)." - загружено ".count($save_data)." позиций из ".$file->num." строк";
			} catch(\Illuminate\Database\QueryException $ex) {
				$importlog->result .= "\n<br />[err] ".date('Y-m-d H:i:s'). " - этап ".$i." из ".count($collection)." ошибка записи в базу ";
				$importlog->has_errors = 1;
				//dd($ex->getMessage());
			}

			Helper::saveImportlogActs($importlog);
		};

		if ($importlog->importtype_id == 1) {
			$importlog->importstage_id = 3;
		} elseif ($importlog->importtype_id == 2 || $importlog->importtype_id == 6) {
			$importlog->importstage_id = 4;
		}
		$importlog->finished_at = date('Y-m-d H:i:s');
		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Задача загрузки данных из файла закончена.";

		Helper::saveImportlogActs($importlog);

		$this->importlogService->processActionResults($auto, 'status', 'Успешно отработано');
    }

	/**
	 * Creates new cats and products in catalogue on tproducts
	 * @param object $importlog
	 * @param string $num_transacts
	*/
	public function createNewProdCats($importlog, $num_transacts)
	{
		$importlog->importstage_id = 2;
		$importlog->started_at = date('Y-m-d H:i:s');

		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Задача добавления новых категорий начата.";
		Helper::saveImportlogActs($importlog);

		$err = array();
		$count = 0;

		$err[] = $this->addNewCats($importlog, $importlog->id);
		$err[] = $this->addNewParentProducts($importlog, $importlog->id, $count, 1000);
		$err[] = $this->addNewProducts($importlog, $importlog->id, $count, 1000);

		if(in_array(1, $err)) {
			$importlog->importstage_id = 3;
		} else {
			$importlog->importstage_id = 4;
		}

		$importlog->finished_at = date('Y-m-d H:i:s');
		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Задача добавления новых позиций закончена.";

		Helper::saveImportlogActs($importlog);

		echo redirect()->route('admin.importlogs.index')
			->with('status','Успешно отработано');
	}

	/**
	 * Syncs tproducts data with products
	 * @param object $importlog
	 * @param string $num_transacts
	 * @param bool $auto
	*/
	public function syncTproducts($importlog, $num_transacts, $auto)
	{
		$importlog->importstage_id = 2;
		$importlog->started_at = date('Y-m-d H:i:s');
		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Задача проверки синхронизируемых данных начата.";

		Helper::saveImportlogActs($importlog);

		if ($importlog->importtype_id == 1) {
			$this->checkNewCats($importlog, $importlog->id);
			$this->checkNewParentProducts($importlog, $importlog->id);
		}

		$this->checkNewProducts($importlog, $auto, $importlog->id);

		$importlog->importstage_id = 2;
		$importlog->started_at = date('Y-m-d H:i:s');
		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Задача обновления данных начата.";

		Helper::saveImportlogActs($importlog);

		if ($importlog->cat_id != 0 && $importlog->importtype_id == 1) {
			$this->deleteOldProducts($importlog, 1000, $importlog->id);
		}

		if ($importlog->importtype_id == 1) {
			$this->updateOldCats($importlog, $importlog->id, 1000);
			$this->updateOldProducts($importlog, $importlog->id, 1000);
		}

		if ($importlog->importtype_id == 2 || $importlog->importtype_id == 6) {
			$this->productService->resetProductsStocks($importlog);
			$this->updateOldProductStocks($importlog, $importlog->id, 1000);
		}

		$importlog->importstage_id = 5;
		$importlog->finished_at = date('Y-m-d H:i:s');
		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Задача обновления данных закончена.";

		Helper::saveImportlogActs($importlog);

		$this->importlogService->processActionResults($auto, 'status', 'Успешно отработано');
	}

	/**
	 * Checks if new cats exist in tproducts
	 * @param object $importlog
	 * @param string $id
	*/
	private function checkNewCats($importlog, $id)
	{
		$new_cats = DB::table('tproducts')
			->leftJoin('cats', 'cats.sku', '=', 'tproducts.cat_sku')
			->select('cats.id')
			->where([
		       	['tproducts.importlog_id', '=', $id],
		       ])
			->whereNull('cats.id')
			->first();

		if($new_cats) {
			$importlog->importstage_id = 3;
			$importlog->started_at = date('Y-m-d H:i:s');
			$importlog->result .= "\n<br />[err] ".date('Y-m-d H:i:s'). " - Найдены новые категории, отсутствующие в базе.";
			$importlog->has_errors = 1;

			Helper::saveImportlogActs($importlog);

			echo redirect()->route('admin.importlogs.index')
				->with('error','Найдены новые категории');
		}
	}

	/**
	 * Checks if new prods exist in tproducts
	 * @param object $importlog
	 * @param string $id
	 * @param bool $auto
	*/
	private function checkNewProducts($importlog, $auto, $id)
	{
		$new_prods = DB::table('tproducts')
			->leftJoin('products', 'products.sku', '=', 'tproducts.sku')
			->select('tproducts.id', 'tproducts.sku', 'tproducts.articul')
			->where([
	        	['tproducts.importlog_id', '=', $id],
	        ])
			->whereNull('products.id')
			->limit(6)
			->get();

		if($new_prods && count($new_prods) >= 1) {
			$importlog->importstage_id = 3;
			$importlog->started_at = date('Y-m-d H:i:s');
			$importlog->result .= "\n<br />[err] ".date('Y-m-d H:i:s'). " - Найдены новые товары, отсутствующие в базе:";
			$importlog->result .= "<ul>";
			$i = 0;
			foreach($new_prods as $new_prod)
			{
				$i++;
				$importlog->result .= "<li>".$new_prod->articul." (".$new_prod->sku.")</li>";
				if ($i == 5) {
					break;
				}
			};
			if (count($new_prods) > 5) {
				$importlog->result .= "<li> + другие</li>";
			}
			$importlog->result .= "</ul>";
			$importlog->has_errors = 1;

			Helper::saveImportlogActs($importlog);
		}
	}

	/**
	 * Checks if new prods exist in tproducts
	 * @param object $importlog
	 * @param string $id
	*/
	private function checkNewParentProducts($importlog, $id)
	{
		$parent_prods = DB::table('tproducts')
			->leftJoin('products', 'products.sku', '=', 'tproducts.parent_sku')
			->leftJoin('cats', 'cats.sku', '=', 'tproducts.cat_sku')
			->select('tproducts.id')
			->where([
	        	['tproducts.importlog_id', '=', $id],
	        	['tproducts.parent_sku', '!=', ''],
	        ])
			->whereNull('products.id')
			->first();

		if($parent_prods) {
			$importlog->importstage_id = 3;
			$importlog->started_at = date('Y-m-d H:i:s');
			$importlog->result .= "\n<br />[err] ".date('Y-m-d H:i:s'). " - Найдены новые привязанные товары, отсутствующие в базе.";
			$importlog->result .= "\n<br />[err] ".date('Y-m-d H:i:s'). " - Задача обновления новых позиций закончена с ошибкой.";
			$importlog->has_errors = 1;

			Helper::saveImportlogActs($importlog);

			echo redirect()->route('admin.importlogs.index')
				->with('error','Найдены новые привязанные товары');
		}
	}

	/**
	 * Adds new cats from tproducts
	 * @param object $importlog
	 * @param string $id
	*/
	private function addNewCats($importlog, $id)
	{
		$new_cats = DB::table('tproducts')
			->leftJoin('cats', 'cats.sku', '=', 'tproducts.cat_sku')
			->select('tproducts.cat_name AS name', 'tproducts.cat_sku AS sku')
			->where([
	        	['tproducts.importlog_id', '=', $id],
	        ])
			->whereNull('cats.id')
			->distinct()
			->get();

		if(count($new_cats) > 0) {
			foreach ($new_cats as $key=> $new_cat)
			{
				$new_cats[$key]->is_active = 1;
				$new_cats[$key]->url = 1;
				$new_cats[$key]->created_at = date('Y-m-d H:i:s');
			};

			try {
				Cat::insert(json_decode(json_encode($new_cats), true));
			} catch(\Exception $e) {
				$importlog->result .= "\n<br />[err] ".date('Y-m-d H:i:s'). " - Ошибка создания новых категорий";
				$importlog->has_errors = 1;
				$err = 1;
			}

			$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Cоздано ".($key+1)." новых категорий";
		} else {
			$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Новых категорий не найдено";
		}

		Helper::saveImportlogActs($importlog);

		if (isset($err)) {
			return $err;
		}
	}

	/**
	 * Adds new parent products from tproducts
	 * @param object $importlog
	 * @param string $id
	 * @param string $count
	 * @param string $num_transacts
	*/
	private function addNewParentProducts($importlog, $id, $count, $num_transacts)
	{
		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Задача синхронизации привязанных товаров начата.";
		Helper::saveImportlogActs($importlog);

		$parent_prods = DB::table('tproducts')
			->leftJoin('products', 'products.sku', '=', 'tproducts.parent_sku')
			->leftJoin('cats', 'cats.sku', '=', 'tproducts.cat_sku')
			->select('tproducts.parent_sku as sku', 'tproducts.parent_name as name', 'cats.id as cat_id')
			->where([
	        	['tproducts.importlog_id', '=', $id],
	        	['tproducts.parent_sku', '!=', ''],
	        ])
			//->whereNull('products.id')
			->distinct()
			->chunkById($num_transacts, function($newpar_prods) use (&$count, $importlog) {
				$count = $count + count($newpar_prods);

				if(count($newpar_prods) > 0) {
					foreach($newpar_prods as $parent_key => $parent_prod)
					{
						try {
							Product::updateOrCreate(
							    ['sku' => $parent_prod->sku],
							    [
							    	'name' => $parent_prod->name,
							    	'cat_id' => $parent_prod->cat_id,
							    	'is_active' => 1,
							    	'parent_id' => 0,
							    	'weight' => 0,
							    	'articul' => ''
							    ]
							);
						} catch(\Exception $e) {
							$importlog->result .= "\n<br />[err] ".date('Y-m-d H:i:s'). " - Ошибка обновления товара ".$parent_prod->sku;
							$importlog->has_errors = 1;
							$err = 1;
						}
					};
				} else {
					$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Новых привязанных товаров больше не найдено.";
				}
		}, 'tproducts.parent_sku', 'sku');
	
		if (isset($err)) {
			return $err;
		}
	}

	/**
	 * Adds new products from tproducts
	 * @param object $importlog
	 * @param string $id
	 * @param string $count
	  * @param string $num_transacts
	*/
	private function addNewProducts($importlog, $id, $count, $num_transacts)
	{
		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Задача добавления новых товаров начата";
		Helper::saveImportlogActs($importlog);

		$n_prods = DB::table('tproducts')
			->leftJoin('products', 'products.sku', '=', 'tproducts.sku')
			->leftJoin('cats', 'cats.sku', '=', 'tproducts.cat_sku')
			->select('tproducts.id', 'tproducts.name', 'tproducts.articul', 'tproducts.sku', 'tproducts.weight', 'tproducts.comment', 'tproducts.quantity', 'tproducts.price', 'tproducts.sort', 'cats.id AS cat_id')
			->whereNull('products.id')
			->where([
	        	['tproducts.importlog_id', '=', $id],
	        ])
			->distinct()
			->chunkById($num_transacts, function($new_prods) use (&$count, $importlog) {
				
				$count = $count + count($new_prods);

				if(count($new_prods) > 0) {
					foreach ($new_prods as $key => $new_prod)
					{
						$new_prods[$key]->is_active = 1;
						$new_prods[$key]->created_at = date('Y-m-d H:i:s');
						$new_prods[$key]->id = 0;
					};

					try {
						$prod_insert = Product::insert(json_decode(json_encode($new_prods), true));
						$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Cоздано ".count($new_prods)." из ".$count." новых товаров";
					} catch(\Exception $e) {
						$importlog->result .= "\n<br />[err] ".date('Y-m-d H:i:s'). " - Ошибка создания ".count($new_prods)." из ".$count." новых товаров";
						$importlog->has_errors = 1;
						$err = 1;
					}

					Helper::saveImportlogActs($importlog);
				} else {
					$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Новых товаров больше не найдено.";
					Helper::saveImportlogActs($importlog);
				}
		}, 'tproducts.id', 'id');
	
		if (isset($err)) {
			return $err;
		}
	}

	/**
	 * Updates cats in catalog by tproducts cat data
	 * @param object $importlog
	 * @param string $id
	 * @param string $num_transacts
	*/
	private function updateOldCats($importlog, $id, $num_transacts)
	{
		$cat_count = 0;

		DB::table('tproducts')
			->leftJoin('cats', 'cats.sku', '=', 'tproducts.cat_sku')
			->select('cats.id', 'tproducts.cat_name AS name')
			->where([
		       	['tproducts.importlog_id', '=', $id],
			])
			->distinct()
			->chunkById($num_transacts, function($cats) use (&$cat_count, $importlog) {
				$cat_count = $cat_count + count($cats);

				if(count($cats) > 0) {
					DB::beginTransaction();
					foreach($cats as $cat) {
						DB::table('cats')
			               ->where('id', '=', $cat->id)
			               ->update([
			                'name' => $cat->name,
			            ]);
			        };
			        DB::commit();

					$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Обновлено ".count($cats)." из ".$cat_count." категорий";

					Helper::saveImportlogActs($importlog);
				}
		}, 'cats.id', 'id');
	}

	/**
	 * Updates products in catalog by tproducts prod data
	 * @param object $importlog
	 * @param string $id
	 * @param string $num_transacts
	*/
	private function updateOldProducts($importlog, $id, $num_transacts)
	{
		$prod_count = 0;

		DB::table('tproducts')
			->leftJoin('cats', 'cats.sku', '=', 'tproducts.cat_sku')
			->leftJoin('products AS products', 'products.sku', '=', 'tproducts.sku')
			->leftJoin('products AS parents', 'parents.sku', '=', 'tproducts.parent_sku')
			->select('products.id AS product_id', 'tproducts.id', 'tproducts.name', 'tproducts.articul', 'tproducts.sku', 'tproducts.weight', 'tproducts.comment', 'tproducts.sort', 'cats.id AS cat_id', 'parents.id AS parent_id')
			->where([
		       	['tproducts.importlog_id', '=', $id],
			])
			->chunkById($num_transacts, function($prods) use (&$prod_count, $importlog) {
				$prod_count = $prod_count + count($prods);

				if(count($prods) > 0) {
					DB::beginTransaction();
					foreach($prods as $prod) {
						$err = 0;
						try {
							DB::table('products')
				               ->where('id', '=', $prod->product_id)
				               ->update([
					               'name' => $prod->name,
					               'articul' => $prod->articul,
					               'weight' => $prod->weight,
					               'comment' => $prod->comment,
					               'cat_id' => $prod->cat_id,
					               'parent_id' => $prod->parent_id ?? 0,
				            ]);
			            } catch(\Exception $e) {
			            	$importlog->result .= "\n<br />[err] ".date('Y-m-d H:i:s'). " - Ошибка обновления товара ".$prod->product_id;
			            	$importlog->has_errors = 1;
			            	$err = 1;
			            }

						if($err == 0) {
							$this->deleteTproduct($prod->id);
			            }
			        };
			        DB::commit();

					$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Обработано ".count($prods)." из ".$prod_count." товаров";

					Helper::saveImportlogActs($importlog);
				}
		}, 'tproducts.id', 'id');
	}

	/**
	 * Updates products stocks in catalog by tproducts prod data
	 * @param object $importlog
	 * @param string $id
	 * @param string $num_transacts
	*/
	private function updateOldProductStocks($importlog, $id, $num_transacts)
	{
		$prod_count = 0;

		DB::table('tproducts')
			->leftJoin('products AS products', 'products.sku', '=', 'tproducts.sku')
			->select('products.id AS product_id', 'tproducts.id', 'tproducts.weight', 'tproducts.price', 'tproducts.quantity')
			->where([
		       	['tproducts.importlog_id', '=', $id],
			])
			->whereNotNull('products.id')
			->chunkById($num_transacts, function($prods) use (&$prod_count, $importlog) {
				$prod_count = $prod_count + count($prods);

				if(count($prods) > 0) {
					DB::beginTransaction();
					foreach($prods as $prod) {
						$err = 0;
						try {
							DB::table('products')
								->where('id', '=', $prod->product_id)
								->update([
									'quantity' => $prod->quantity,
									'price' => $prod->price,
									'weight' => $prod->weight,
				            ]);
			            } catch(\Exception $e) {
			            	$importlog->result .= "\n<br />[err] ".date('Y-m-d H:i:s'). " - Ошибка обновления товара ".$prod->product_id;
			            	$importlog->has_errors = 1;
			            	$err = 1;
			            }

						if($err == 0) {
							$this->deleteTproduct($prod->id);
			            }
			        };
			        DB::commit();

					$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Обработано ".count($prods)." из ".$prod_count." товаров";

					Helper::saveImportlogActs($importlog);
				}
		}, 'tproducts.id', 'id');
	}

	/**
	 * Deletes old products from catalog by cat_id
	 * @param object $importlog
	 * @param string $num_transacts
	*/
	private function deleteOldProducts($importlog, $num_transacts, $id)
	{
		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Удаление старых данных по категории начато";

		//get all parent_id prods with no prods connected
		 $parent_newprods = DB::table('products')
			->leftJoin('tproducts', function ($join) use ($id) {
				$join->on('products.sku', '=', 'tproducts.parent_sku')
					->where('tproducts.importlog_id', '=', $id);
				})
			->select('products.id')
			->where([
				['products.cat_id', '=', $importlog->cat_id],
				['products.parent_id', '=', 0],
			])
			->whereNotNull('tproducts.id')
			->distinct()
			->pluck('products.id')->toArray();

			$oldprod_count = 0;

			DB::table('products')
				->leftJoin('tproducts', 'tproducts.sku', '=', 'products.sku')
				->leftJoin('imgs', function ($join) {
						$join->on('imgs.imgable_id', '=', 'products.id')
							->where('imgs.imgable_type', '=', 'App\Models\Product');
		        })
				->select('products.id','products.articul','products.sku','imgs.url','imgs.id AS img_id')
				->where([
		        	['products.cat_id', '=', $importlog->cat_id],
		        ])
		        ->whereNotIn('products.id', $parent_newprods)
				->whereNull('tproducts.id')
				->chunkById($num_transacts, function($old_prods) use (&$oldprod_count, $importlog) {
					$this->productService->deleteOldProducts($importlog, $old_prods, $oldprod_count, $importlog->id);
				}, 'products.id', 'id');

		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Удаление старых данных по категории завершено";

		Helper::saveImportlogActs($importlog);
	}

	/**
	 * Deletes old products from catalog by cat_id
	 * @param string $id
	*/
	private function deleteTproduct($id)
	{
		try {
			DB::table('tproducts')->where('id', '=', $id)->delete();
		} catch(\Illuminate\Database\QueryException $ex) {
			
		}
	}
}
