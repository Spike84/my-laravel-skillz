<?php

namespace App\Jobs;

use App\Models\Mailing;
use App\Models\Mailuser;
use App\Notifications\MassMailNotify;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;
use Notification;

class MassMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $mailuser;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mailuser)
    {
        $this->mailuser = $mailuser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $message = array();
        $message['old'] = preg_split('/(\[.+?\].+?\[\/.+?\])/m', $this->mailuser->message, 0, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        $message['new'] = "";

        foreach($message['old'] as $m_key => $m_val)
        {
        	//echo strpos($m_val, '[url=');
        	if (strpos($m_val, '[url=') === 0) {
        		$url = array();

       			$url['end'] = str_replace('[/url]','',$m_val);
       			$url['end'] = str_replace('[url=','',$url['end']);
       			$url['end'] = substr($url['end'], (strpos($url['end'], ']')+1));

       			$url['link'] = substr($m_val, (strpos($m_val, '[url=')+5), (strpos($m_val, ']') - (strpos($m_val, '[url=')+5)));

       			$url['parsed_lnk'] = parse_url($url['link']);
       			$url['new_link'] = "";

       			if (isset($url['parsed_lnk']['scheme'])) {
       				$url['new_link'] .= $url['parsed_lnk']['scheme']."://";
       			}
       			if (isset($url['parsed_lnk']['host'])) {
       				$url['new_link'] .= $url['parsed_lnk']['host']."/mmail";
       			}
       			if (isset($url['parsed_lnk']['path'])) {
       				$url['new_link'] .= $url['parsed_lnk']['path'];
       			}
       			if (isset($url['parsed_lnk']['query'])) {
       				$url['new_link'] .= "?".$url['parsed_lnk']['query']."&mailing=".$this->mailuser->id;
       			} else {
       				$url['new_link'] .= "?mailing=".$this->mailuser->id;
       			}

       			$message['new'] .= "<a href='".$url['new_link']."'>".$url['end']."</a>";
       		} else {
       			$message['new'] .= $m_val;
       		}
       	};

       	$this->mailuser->message = $message['new'];

       	//dd($message);

		try {
			Notification::route('mail', $this->mailuser->email)
	    		->notify(new MassMailNotify($this->mailuser));

	    	Mailuser::where('id', '=', $this->mailuser->id)
				->update(
				['is_sent' => 1],
			);
    	} catch(\Exception $e) {
			//dd($e);
		}
    }
}
