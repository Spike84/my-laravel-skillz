<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Jobs\MassMailJob;
use App\Models\Mailuser;
use Illuminate\Http\Request;
use DB;

class MailusersController extends Controller
{
    public function index()
    {
        $num_transacts = 500;
        $limit = 200;

        DB::table('mailusers')
			->select('mailusers.id', 'mailusers.fio', 'mailusers.email','mailings.subject','mailings.message')
			->leftJoin('mailings', 'mailings.id', '=', 'mailusers.mailing_id')
			->where([
	        	['mailings.is_sent', '=', 1],
	        	['mailusers.is_sent', '=', 0],
	       	])
	       	->limit($limit) 
			->chunkById($num_transacts, function($mailusers) use (&$count, $limit) {
				$count = $count + count($mailusers);

				if ($count > $limit) {
					die();
				}

				if(count($mailusers) > 0) {
					foreach($mailusers as $mailuser)
					{
						dispatch(new MassMailJob($mailuser));
						usleep(300000);
					};

				}
		}, 'mailusers.id', 'id');
    }

    public function edit(Request $request)
    {
    	$url = str_replace("/mmail","",$_SERVER['REQUEST_URI']);

    	$mailing = $request->only(['mailing']);

    	if (is_numeric($mailing['mailing'])) {
    		$url = str_replace("?mailing=".$mailing['mailing'],"",$url);
    		$url = str_replace("&mailing=".$mailing['mailing'],"",$url);
    		
	    	if (empty($url)) {
	    		$url = "/";
	    	}

    		try {
    			Mailuser::where('id', '=', $mailing['mailing'])
					->update(['url' => $url]);
    		} catch(\Illuminate\Database\QueryException $ex) {

	        }
    	}

    	return redirect($url);
    }
}
