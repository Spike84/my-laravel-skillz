<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Cat;
use App\Models\Product;
use App\Models\Prop;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Auth;

class ProductsController extends Controller
{
	protected $productService;

	function __construct(ProductService $productService)
    {
         $this->productService = $productService;
    }

    /**
	 * Shows Product's list info
	 * @param Request $request
	*/
    public function index(Request $request)
    {
    	$request->validate([
            'cat_id' => 'nullable|numeric',
            'in_stock' => 'nullable|numeric',
            'prop' => 'nullable|array',
            'prop.*' => 'nullable|array',
            'prop.*.*' => 'nullable|numeric',
        ]);

		//dd($request);

    	$cat = array();
    	$cond = array();
    	$cond[] = ['products.is_active', '=', 1];

    	$input = $request->query();
    	
    	if((isset($input['prop']) && !empty($input['prop'])) || (isset($input['searchw']) && !empty($input['searchw'])) || (isset($input['in_stock']) && ($input['in_stock'] == 1)) ) {
    		//$cond[] = ['products.parent_id', '!=', 0];
    	} else {
    		$cond[] = ['products.parent_id', '=', 0];
    	}

    	if (isset($input['searchw'])) {
    		$cond[] = ['products.name', 'like', '%'.$input['searchw'].'%'];
    	}
    	if (isset($input['cat_id']) && !empty($input['cat_id'])) {
    		$cat = Cat::select('id','name')
    			->findorfail($input['cat_id']);
    		$cond[] = ['products.cat_id', '=', $input['cat_id']];
    	}
    	if (isset($input['in_stock']) && ($input['in_stock'] == 1)) {
    		$cond[] = ['products.quantity', '>', 0];
    	}

		$products = Product::with(['cat', 'imgs' => function ($query) {
				$query->where('imgs.is_main', '=', 1)
					->orderBy('is_main');
				}
			])
			->withCount('children')
			->sortable();

		if (isset($input['prop']) && !empty($input['prop'])) {
			foreach($input['prop'] as $prop_key => $prop_val)
			{
				$products = $products->join('propvals as prop'.$prop_key, function ($join) use (&$prop_key) {
		            $join->on('products.id', '=', 'prop'.$prop_key.'.product_id')
						->where('prop'.$prop_key.'.prop_id', '=', $prop_key);
		        });

				$joinIn = array();

				foreach($prop_val as $kkey => $prop_value)
				{
					$joinIn[] = $prop_value;
				};
				$products = $products->whereIn('prop'.$prop_key.'.propvalue_id', $joinIn);
			};
		}

		$products = $products->where($cond)->paginate(18);

		//dump($products);

		$here = "products";

    	return view('products.index', compact('products','input','here','cat'));
    }

	/**
	 * Shows Product's detail info
	 * @param string $id
	*/
	public function show($id)
    {
	    $product = Product::with(['propvals', 
	    	'imgs' => function ($query) {
				$query->orderBy('is_main', 'desc');
			}])
			->withCount('children')
	    	->where([
	        	['products.is_active', '=', 1],
	        	['products.parent_id', '=', 0]
	        ])
	        ->findorfail($id);

		$addprods = array();

		if ($product->children->count() != 0) {
	        $addprods = $this->productService->formProductAddprods($id);
		}

		$props = Prop::all('id','name')
			->keyBy('id')
			->toArray();

		$here = "products";

        return view('products.show',compact('product', 'props', 'here', 'addprods'));
    }

}
