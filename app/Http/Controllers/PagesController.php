<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = Page::where('url', 'index')->firstOrFail();
        $here = "pages-index";

        return view('pages.index',compact('page','here'));
    }

	public function view($url)
    {
        $page = Page::where('url', $url)->firstOrFail();
        $here = "pages-".$page->url;

        return view('pages.index',compact('page','here'));
    }

}
