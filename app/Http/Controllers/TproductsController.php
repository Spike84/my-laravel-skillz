<?php

namespace App\Http\Controllers;

use App\Models\Importlog;
use App\Models\Importtype;
use App\Models\Tproduct;
use App\Services\ImportlogService;
use App\Services\TproductService;
use Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TproductsController extends Controller
{
	protected $importlogService;
	protected $tproductService;

	function __construct(ImportlogService $importlogService, TproductService $tproductService)
    {
         $this->importlogService = $importlogService;
         $this->tproductService = $tproductService;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$auto_load = $this->importlogService->checkSettingParam(1);
    	if (!$auto_load) {
    		die();
    	}

        $importtype = Importtype::findOrFail(6);

		$importlog = $this->importlogService->createImportlog($importtype, 0, true);
		$this->tproductService->createTproducts($importlog, 1000, true);
		$this->tproductService->syncTproducts($importlog, 1000, true);

		Storage::disk('import')->delete($importtype->url.'/'.$importtype->file);
    }
}
