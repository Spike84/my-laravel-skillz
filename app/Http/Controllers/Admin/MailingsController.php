<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\MassMailJob;
use App\Http\Controllers\Controller;
use App\Models\Mailing;
use App\Models\Mailuser;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use DB;

class MailingsController extends Controller
{

	function __construct()
    {
         $this->middleware('permission:mailing-list', ['only' => ['index','show']]);
         $this->middleware('permission:mailing-create', ['only' => ['create','store','edit','update','build','arrange','destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mailings = Mailing::select('id','name','is_sent','created_at')
        	->withCount('mailusers')
        	->sortable()
        	->paginate(20);

        return view('admin.mailings.index',compact('mailings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.mailings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'subject' => 'required|string',
            'message' => 'required'
        ]);

        $input = $request->all();
        $input['is_sent'] = 0;
        $input['log'] = date('Y-m-d H:i:s'). " - Рассылка создана пользователем ".Auth::user()->name;

		try {
			$mailing = Mailing::create($input);
			return redirect()->route('admin.mailings.index')
				->with('status','Успешно создано');
		} catch(\Illuminate\Database\QueryException $ex) {
        	return redirect()->route('admin.mailings.index')
        		->with('error','Ошибка создания');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mailing  $mailing
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mailing = Mailing::findOrFail($id);

        return view('admin.mailings.show',compact('mailing'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mailing  $mailing
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mailing = Mailing::where([
	        	['is_sent', '=', 0]
	        ])
        	->findOrFail($id);

        return view('admin.mailings.edit',compact('mailing'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mailing  $mailing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string',
            'subject' => 'required|string',
            'message' => 'required'
        ]);

        $mailing = Mailing::where([
	        	['is_sent', '=', 0]
	        ])
        	->findOrFail($id);

        $input = $request->all();
        $input['is_active'] = 0;

		try {
			$mailing->update($input);
			return redirect()->route('admin.mailings.index')
			->with('status','Успешно обновлено');
		} catch(\Illuminate\Database\QueryException $ex) {
        	return redirect()->route('admin.mailings.index')
        		->with('error','Ошибка обновления');
        }
    }

	public function build($id)
    {
    	$mailing = Mailing::select('id','log')
    		->withCount('mailusers')
    		->where([
	        	['is_sent', '=', 0]
	        ])
        	->findOrFail($id);

        if($mailing->mailusers_count > 0) {
        	return redirect()->route('admin.mailings.index')
        		->with('error','В рассылке есть подписчики');
        }

        $users = User::select('id','fio','email')
        	->where([
	        	['is_mailable', '=', 1]
	        ])
        	->get()
        	->toArray();

		foreach($users as $user_key => $user_val) 
		{
			unset($users[$user_key]['id']);
			$users[$user_key]['user_id'] = $user_val['id'];
			$users[$user_key]['mailing_id'] = $id;
			$users[$user_key]['is_sent'] = 0;
		};

        try {
			Mailuser::insert($users, true);

			$mailing->log .= "\n<br />".date('Y-m-d H:i:s'). " - Сформирован список из ".count($users)." пользователей рассылки пользователем ".Auth::user()->name;

			Mailing::where('id', '=', $id)
				->update(['log' => $mailing->log]);

			return redirect()->route('admin.mailings.index')
				->with('status','Успешно отработано');
		} catch(\Exception $e) {
			return redirect()->route('admin.mailings.index')
				->with('error','Ошибка формирования');
		}
    }

	public function arrange($id)
    {
    	$mailing = Mailing::withCount('mailusers')
    		->where([
	        	['is_sent', '=', 0]
	        ])
        	->findOrFail($id);

        if($mailing->mailusers_count == 0) {
        	return redirect()->route('admin.mailings.index')
        		->with('error','В рассылке нет подписчиков');
        }

		$mailing->log .= "\n<br />".date('Y-m-d H:i:s'). " - Рассылка начата пользователем ".Auth::user()->name;

		 try {
			Mailing::where('id', '=', $id)
				->update(
					['log' => $mailing->log, 'is_sent' => 1]
				);
			return redirect()->route('admin.mailings.index')
				->with('status','Успешно выполнено');
		} catch(\Exception $e) {
			return redirect()->route('admin.mailings.index')
				->with('error','Ошибка записи лога');
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mailing  $mailing
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mailing = Mailing::where([
	        	['is_sent', '=', 0]
	        ])
        	->findOrFail($id);

        try {
			$mailing->delete();
			return redirect()->route('admin.mailings.index')
				->with('status','Успешно удалено');
		} catch(\Illuminate\Database\QueryException $ex) {
        	return redirect()->route('admin.mailings.index')
        		->with('error','Ошибка удаления');
        }
    }
}
