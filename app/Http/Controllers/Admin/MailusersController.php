<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Mailuser;
use App\Models\Mailing;
use Illuminate\Http\Request;

class MailusersController extends Controller
{
	function __construct()
    {
         $this->middleware('permission:mailing-list', ['only' => ['index']]);
         $this->middleware('permission:mailing-create', ['only' => ['create','store','destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        $mailing = Mailing::select('id','name')
        	->findOrFail($id);

		$input = $request->query();

        if (isset($input['searchw'])) {
        	$mailusers = Mailuser::sortable()
        		->where([
        			['mailusers.fio', 'like', '%'.$input['searchw'].'%'],
        			['mailusers.mailing_id', '=', $id]
        		])
        		->orWhere('mailusers.email', 'like', '%'.$input['searchw'].'%')
        		->paginate(20);
        } else {
        	$mailusers = Mailuser::sortable()
        		->where([
        			['mailusers.mailing_id', '=', $id]
        		])
        		->paginate(20);
        }

        return view('admin.mailusers.index',compact('mailing','mailusers','input'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mailuser  $mailuser
     * @return \Illuminate\Http\Response
     */
    public function show(Mailuser $mailuser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mailuser  $mailuser
     * @return \Illuminate\Http\Response
     */
    public function edit(Mailuser $mailuser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mailuser  $mailuser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mailuser $mailuser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mailuser  $mailuser
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mailuser = Mailuser::select('id', 'mailing_id')
        	->where([
        			['is_sent', '!=', 1]
        		])
        	->findOrFail($id);

        try {
			$mailuser->delete();
			return redirect()->route('admin.mailusers.index', $mailuser->mailing_id)
				->with('status','Успешно удалено');
		} catch(\Illuminate\Database\QueryException $ex) {
        	return redirect()->route('admin.mailusers.index', $mailuser->mailing_id)
        		->with('error','Ошибка удаления');
        }
    }
}
