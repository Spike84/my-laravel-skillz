<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
	function __construct()
    {
         $this->middleware('permission:change-settings', ['only' => ['index','edit']]);
    }

    /**
     * Display a listing of the settings
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::get();

        return view('admin.settings.index',compact('settings'));
    }

    /**
     * Edit settings value
     *
     * @param integer $id
     * @param bool $is_active
     */
    public function edit($id, $is_active)
    {
        $setting = Setting::findOrFail($id);

        $setting->is_active = $is_active;

        try {
        	$setting->save();
        	return true;
        } catch(\Illuminate\Database\QueryException $ex) {
        	return false;
        }
    }
}
