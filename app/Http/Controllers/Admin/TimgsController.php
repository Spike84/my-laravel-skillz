<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Img;
use App\Models\Importlog;
use App\Models\Importtype;
use App\Models\Product;
use App\Models\Timg;
use App\Services\TimgService;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Helper;

class TimgsController extends Controller
{
	protected $timgService;

	function __construct(TimgService $timgService)
    {
         $this->middleware('permission:img-list', ['only' => ['index']]);
         $this->middleware('permission:img-create', ['only' => ['create', 'store', 'edit', 'clear']]);
         $this->middleware('permission:img-delete', ['only' => ['destroy', 'del_files']]);

         $this->timgService = $timgService;
    }

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
		$importlog = Importlog::findOrFail($id);

		$input = $request->query();

        if (isset($input['searchw'])) {
        	$timgs = Timg::sortable()
        		->where([
        			['timgs.sku', 'like', '%'.$input['searchw'].'%'],
        			['timgs.importlog_id', '=', $id]
        		])
        		->paginate(20);
        } else {
        	$timgs = Timg::sortable()
        	->where([
        		['timgs.importlog_id', '=', $id]
        	])
        	->paginate(20);
        }
        

        return view('admin.timgs.index',compact('timgs', 'importlog','input'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $importlog = Importlog::where([
        		['importstage_id', '=', 1]
        	])
    		->findOrFail($id);

        $importlog->importstage_id = 2;
		$importlog->started_at = date('Y-m-d H:i:s');
		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Задача загрузки фото из директории начата.";

		$file = Helper::import_file_inf($importlog->importtype->url, $importlog->importtype->file);

		$importlog->result .= $file->result;

		if ($file->ready == 0) {
			$importlog->importstage_id = 1;
			$importlog->finished_at = date('Y-m-d H:i:s');

			$importlog->save();

			return redirect()->route('admin.importlogs.index')
				->with('error',$file->result);
		}

		$importlog->save();

		$quant = "100";
		$i = 0;

		if(!empty($file->dirs)) {
			$collection = collect($file->dirs)->chunk($quant);
		} else {
			$collection = collect($file->conts)->chunk($quant);
		}

		foreach ($collection as $data)
		{
			$i++;
			$a = 0;
			$save_data = array();

			foreach($data as $key => $val)
			{
				if ($importlog->importtype_id == 4) {
					
					$val = pathinfo($val);
					$save_data[$key]['sku'] = $val['filename'];
					$save_data[$key]['file'] = $val['basename'];
					$save_data[$key]['url'] = $val['dirname'];
					$save_data[$key]['created_at'] = date('Y-m-d H:i:s');
					$save_data[$key]['importlog_id'] = $id;
				} else {
					$images = Storage::disk('import')->files($val);

					foreach($images as $img)
					{
						$a++;
						$img = pathinfo($img);

						$save_data[$a]['sku'] = pathinfo($img['dirname'])['filename'];
						$save_data[$a]['file'] = $img['basename'];
						$save_data[$a]['url'] = $img['dirname'];
						$save_data[$a]['created_at'] = date('Y-m-d H:i:s');
						$save_data[$a]['importlog_id'] = $id;
					};
				}
			};

			try {
				Timg::insert($save_data);
				$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - этап ".$i." из ".count($collection)." - загружено ".count($save_data)." позиций из ".$file->num." файлов";
			} catch(\Illuminate\Database\QueryException $ex) {
				$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - этап ".$i." из ".count($collection)." ошибка записи в базу ";
				//echo $ex->getMessage();
			}

			$importlog->save();

		};

		$importlog->importstage_id = 3;
		$importlog->finished_at = date('Y-m-d H:i:s');
		$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - Задача загрузки изображений из директории закончена.";

		$importlog->save();

		return redirect()->route('admin.importlogs.index')->with('status','Успешно отработано');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Timg  $timg
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	
    }

    /**
     * Syncs main photos
     *
     * @param  \App\Models\Timg  $timg
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $importlog = Importlog::withCount('timgs')
        	->where([
        		['importstage_id', '=', 3]
        	])
    		->findOrFail($id);

		if($importlog->timgs_count == 0) {
			return redirect()->route('admin.importlogs.index')
				->with('error','Не найдено элементов в выгрузке');
		}

        $this->timgService->syncMainPhotos($importlog, 500);

		return redirect()->route('admin.importlogs.index')->with('status','Успешно отработано');
    }

    /**
     * Syncs additional photos.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
		$importlog = Importlog::withCount('timgs')
        	->where([
        		['importstage_id', '=', 3]
        	])
    		->findOrFail($id);

		if ($importlog->timgs_count == 0) {
			return redirect()->route('admin.importlogs.index')
				->with('error','Не найдено элементов в выгрузке');
		}

		$this->timgService->syncAdditPhotos($importlog, 500);

		return redirect()->route('admin.importlogs.index')->with('status','Успешно отработано');
    }

	/**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Timg  $timg
     * @return \Illuminate\Http\Response
     */
    public function del_files()
    {
    	$importtype = Importtype::select('url')
    		->findOrFail(4);

		$file = Helper::import_file_inf($importtype->url, '');
		
		if ($file->ready == 0) {
			return redirect()->route('admin.pages.show')->with('status','Нет изображений для очистки');
		}

		try {
			Storage::disk('import')->deleteDirectory($importtype->url);
			Storage::disk('import')->makeDirectory($importtype->url);

			return redirect()->route('admin.pages.show')
				->with('status','Файлы изображений успешно очищены');
		} catch(\Exception $e) {
			return redirect()->route('admin.pages.show')
				->with('error','Ошибка очистки файлов изображений');
		}
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Timg  $timg
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $timg = Timg::with('importlog:id,importtype_id')
        	->findOrFail($id);

		$del_timg = $this->timgService->deleteTimg($timg, false);

		if (!$del_timg) {
			return redirect()->route('admin.timgs.index', $timg->importlog_id)
				->with('error', 'Ошибка удаления');
		}

		return redirect()->route('admin.timgs.index', $timg->importlog_id)
			->with('status', $del_timg);
    }
}
