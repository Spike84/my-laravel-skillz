<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cat;
use App\Models\Importlog;
use App\Models\Importtype;
use App\Services\ImportlogService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Helper;

class ImportlogsController extends Controller
{
	protected $importlogService;

    function __construct(ImportlogService $importlogService)
    {
         $this->middleware('permission:importlog-list', ['only' => ['index','show']]);
         $this->middleware('permission:importlog-create', ['only' => ['create','store']]);
         $this->middleware('permission:importlog-edit', ['only' => ['edit','update','sync']]);
         $this->middleware('permission:importlog-delete', ['only' => ['destroy', 'clear_all']]);

         $this->importlogService = $importlogService;
    }

	/**
	 * List of importlogs
	*/
    public function index()
    {
    	$importlogs = Importlog::with('user:id,name','importstage','importtype','cat:id,name')
    		->sortable()
    		->paginate(20);
        
        return view('admin.importlogs.index',compact('importlogs'));
    }

	 /**
	 * Shows form to creating new importlog
	*/
    public function create()
    {
    	$importtypes = Importtype::select('id', 'name')
    		->where([
				['id', '<>', 6]
			])
    		->get();
    	$cats = Cat::select('id','name')
    		->orderBy('name', 'asc')
    		->get();

        return view('admin.importlogs.create',compact('importtypes','cats'));
    }

    /**
	 * Saves data to importlog
	 * @param Request $request
	*/
    public function store(Request $request)
    {
        $request->validate([
            'importtype_id' => 'required|numeric',
            'cat_id' => 'required|numeric',
        ]);

        $input = $request->all();

        $importtype = Importtype::where([
				['id', '<>', 6]
			])
        	->findOrFail($input['importtype_id']);
        
        if ($input['cat_id'] != 0) {
        	$cat = Cat::findOrFail($input['cat_id']);
        }

		$this->importlogService->createImportlog($importtype, $input['cat_id'], false);
    }

	/**
	 * Show Importlog on given ID
	 * @param string $id
	*/
    public function show($id)
    {
        $importlog = Importlog::with('user:id,name','importstage','importtype','cat:id,name')
    		->findOrFail($id);

		return view('admin.importlogs.show',compact('importlog'));
    }

	/**
	 * Deletes all importlogs
	*/
	public function clear_all()
    {
    	try {
	    	$importlogs = Importlog::where([
			        ['importstage_id', '<>', 2]
				])
				->delete();

			return redirect()->route('admin.pages.show')
				->with('status','Все задачи выгрузки были успешно удалены');
		} catch(\Exception $e) {
			return redirect()->route('admin.pages.show')
				->with('error','Ошибка удаления задач выгрузки');
		}
    }

	/**
	 * Delete Importlog
	 * @param string $id
	*/
    public function destroy($id)
    {
    	$importlog = Importlog::select('id','importstage_id');
    	if (!Auth::user()->hasRole('admin')) {
	        $importlog = $importlog->where([
		        	['importstage_id', '<>', 2]
		        ]);
        }
        $importlog = $importlog->findOrFail($id);

		try {
			$importlog->delete();
			return redirect()->route('admin.importlogs.index')
				->with('status','Успешно удалено');
		} catch(\Exception $e) {
			return redirect()->route('admin.importlogs.index')
				->with('error','Ошибка удаления');
		}
    }
}
