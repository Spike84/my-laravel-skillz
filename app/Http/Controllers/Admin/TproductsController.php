<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tproduct;
use App\Models\Importlog;
use App\Services\ImportlogService;
use App\Services\TproductService;
use Illuminate\Http\Request;

class TproductsController extends Controller
{
	protected $tproductService;
	protected $importlogService;

    function __construct(TproductService $tproductService, ImportlogService $importlogService)
    {
         $this->middleware('permission:tproduct-list', ['only' => ['index','show']]);
         $this->middleware('permission:tproduct-create', ['only' => ['create','store']]);
         $this->middleware('permission:tproduct-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:tproduct-delete', ['only' => ['destroy']]);

         $this->tproductService = $tproductService;
         $this->importlogService = $importlogService;
    }

	/**
	 * List of importlogs
	 * @param string $id
	 * @param Request $request
	*/
    public function index(Request $request, $id)
    {
        $input = $request->query();

		$importlog = Importlog::findOrFail($id);

		$tproducts = Tproduct::with(['product','importlog','cat','product.cat','parent']);

        if (isset($input['type']) && $input['type'] == 2) {
        	$tproducts
        		->select('products.id as product_id','tproducts.*')
        		->where([
			       	['tproducts.importlog_id', '=', $importlog->id],
			    ])
        		->leftJoin('products', 'products.sku', '=', 'tproducts.sku')
        		->whereNull('products.id');
        } elseif (isset($input['type']) && $input['type'] == 3) {
        	$tproducts
        		->select('products.id as product_id','tproducts.*')
        		->where([
			       	['tproducts.importlog_id', '=', $importlog->id],
			       	['tproducts.parent_sku', '!=', '']
			    ])
        		->leftJoin('products', 'products.sku', '=', 'tproducts.parent_sku')
        		->whereNull('products.id');
        } elseif (isset($input['type']) && $input['type'] == 4) {
        	$tproducts
        		->select('cats.id as cat_id','tproducts.*')
        		->where([
			       	['tproducts.importlog_id', '=', $importlog->id],
			       	['tproducts.cat_sku', '!=', '']
			    ])
        		->leftJoin('cats', 'cats.sku', '=', 'tproducts.cat_sku')
        		->whereNull('cats.id');
        }elseif (isset($input['searchw'])) {
        	$tproducts = $tproducts->where([
			       	['tproducts.importlog_id', '=', $importlog->id],
			       	['tproducts.name', 'like', '%'.$input['searchw'].'%']
			    ])
	        	->orWhere('tproducts.articul', 'like', '%'.$input['searchw'].'%')
	        	->orWhere('tproducts.sku', 'like', '%'.$input['searchw'].'%')
	        	->orWhere('tproducts.parent_sku', 'like', '%'.$input['searchw'].'%');
    	} else {
        	$tproducts = $tproducts->where([
				['tproducts.importlog_id', '=', $importlog->id]
			]);
        }

        $tproducts = $tproducts->sortable()
        	->paginate(20);

        return view('admin.tproducts.index', compact('tproducts', 'importlog', 'input'));
    }

	/**
	 * Creates new tproducts of given importlog
	 * @param string $id
	*/
    public function create($id)
    {
    	$auto_load = $this->importlogService->checkSettingParam(1, false);
    	if ($auto_load) {
    		return redirect()->route('admin.importlogs.index')
				->with('error', 'В настройках включен параметр "'.$auto_load.'" сперва нужно отключить его');
    	}

    	$importlog = Importlog::where([
        		['importstage_id', '=', 1]
        	])
    		->findOrFail($id);

		$this->tproductService->createTproducts($importlog, 1000, false);
    }

	/**
	 * Creates new products cats, etc. of given importlog
	 * @param string $id
	*/
    public function edit($id)
    {
    	$auto_load = $this->importlogService->checkSettingParam(1, false);
    	if ($auto_load) {
    		return redirect()->route('admin.importlogs.index')
				->with('error', 'В настройках включен параметр "'.$auto_load.'" сперва нужно отключить его');
    	}

        $importlog = Importlog::withCount('tproducts')
        	->where([
        		['importstage_id', '=', 3]
        	])
    		->findOrFail($id);

		if($importlog->tproducts_count == 0) {
			echo redirect()->route('admin.importlogs.index')
				->with('error','Не найдено элементов в выгрузке');
		}

		$this->tproductService->createNewProdCats($importlog, 1000);
    }

	/**
	 * syncs tproducts of given importlog
	 * @param string $id
	*/
	public function sync($id)
    {
    	$auto_load = $this->importlogService->checkSettingParam(1, false);
    	if ($auto_load) {
    		return redirect()->route('admin.importlogs.index')
				->with('error', 'В настройках включен параметр "'.$auto_load.'" сперва нужно отключить его');
    	}

		$importlog = Importlog::withCount('tproducts')
        	->where([
        		['importstage_id', '=', 4]
        	])
    		->findOrFail($id);

		if($importlog->tproducts_count == 0) {
			echo redirect()->route('admin.importlogs.index')
				->with('error','Не найдено элементов в выгрузке');
		}

		$this->tproductService->syncTproducts($importlog, 1000, false);
	}

	/**
	 * delete tproduct
	 * @param string $id
	*/
    public function destroy($id)
    {
		$tproduct = Tproduct::select('id', 'importlog_id')
			->findOrFail($id);

		try {
			$tproduct->delete();
			return redirect()->route('admin.tproducts.index', $tproduct->importlog_id)
				->with('status','Успешно удалено');
		} catch(\Illuminate\Database\QueryException $ex) {
			return redirect()->route('admin.tproducts.index', $tproduct->importlog_id)
				->with('error','Ошибка удаления');
		}
    }
}
