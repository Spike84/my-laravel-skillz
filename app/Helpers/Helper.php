<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use App\Models\Importlog;

class Helper
{
	/**
	 * Checks if file exists and parses it
	 * @param string $dir
	 * @param string $file
	*/
	public static function import_file_inf($dir, $file)
    {
        $obj = new Helper();
        
        $obj->type = 'файл';

		if(empty($file)) {
			$obj->type = 'директория';
		}

        if (Storage::disk('import')->exists($dir.$file)) {
        	$obj->ready = 1;

        	if($obj->type == 'файл') {
        		$obj->conts = file(Storage::disk('import')->path($dir.$file));
        		$obj->num = (count($obj->conts)-1);
        	} elseif ($obj->type == 'директория') {
        		$obj->dirs = Storage::disk('import')->directories($dir);
        		$obj->conts = Storage::disk('import')->files($dir);
        		if (!empty($obj->dirs)) {
        			$obj->num = count($obj->dirs);
        		} else {
        			$obj->num = count($obj->conts);
        		}
        	}

        	if ($obj->num == 0) {
	        	$obj->ready = 0;
	        	$obj->result = "\n<br />".date('Y-m-d H:i:s'). " - ".$obj->type." по пути ".$dir.$file." не содержит элементов для выгрузки";
	        } else {
	        	$obj->result = "\n<br />".date('Y-m-d H:i:s'). " - ".$obj->type." по пути ".$dir.$file." содержит ".$obj->num." элементов для выгрузки";
	        }

        } else {
        	$obj->ready = 0;
        		$obj->result = "\n<br />".date('Y-m-d H:i:s'). " - не найдено: ".$obj->type." по пути ".$dir.$file;
        }
        
        return $obj;
    }

	/**
	 * Checks if resized image exists and creates it if not
	 * @param string $file
	 * @param string $width
	*/
	public static function image_resizer($file, $width)
    {
    	if (Storage::disk('public')->exists($file)) {
		    $fileinf = pathinfo($file);
		    
		    $res_file = $fileinf['dirname']."/resized/".$fileinf['filename']."/".$fileinf['filename']."x".$width.".".$fileinf['extension'];

		    if (Storage::disk('public')->exists($res_file)) {
		    	return Storage::disk('public')->url($res_file);
		    } else {
		    	Storage::disk('public')->copy($file, $res_file);
		    	
		    	$base_file = Storage::disk('public')->path($file);
		    	$new_file = Storage::disk('public')->path($res_file);
		    	
		    	$imgsze = getimagesize($base_file);

		    	if($imgsze[0] <= $width) {
		    		return Storage::disk('public')->url($res_file);
		    	} else {
			    	if ($imgsze['mime'] == "image/jpeg" || $imgsze['mime'] == "image/pjpeg") {
			    		$im = imagecreatefromjpeg($base_file);
			    		$new_height = round($width*$imgsze[1]/$imgsze[0]);
			    		$dest = imagecreatetruecolor($width,$new_height);
			    		imagecopyresampled($dest, $im, 0, 0, 0, 0, $width, $new_height, $imgsze[0], $imgsze[1]);

						$mimg = imagejpeg($dest, $new_file, '100');
						//$mimg = Storage::disk('public')->put($new_file, $dest);
						imagedestroy($dest);
						imagedestroy($im);
			    	} elseif ($imgsze['mime'] == "image/gif") {
			    		//$im = imagecreatefromgif($base_file);

			    		
			    	} elseif ($imgsze['mime'] == "image/png") {
			    		//$im = imagecreatefrompng($base_file);

			    		
			    	}
			    	
			    	return Storage::disk('public')->url($res_file);
		    	}

		    }
		    //print_r($fileinf);
		    //return $res_file;
		} else {
			return "файл не найден";
		}
    }

	/**
	 * Checks if resized image exists and creates it if not
	 * @param object $importlog
	 * @param array $parsed_params
	 * @param array $data
	*/
	public static function csv_main_parser($importlog, $parsed_params, $data)
    {
    	$helper = new Helper();

    	$save_data = array();

		foreach($data as $key => $val) 
		{
			$val = explode(";", $val);

			if(count($val) > 1) {
				$err = 0;

				foreach($val as $kk => $vv)
				{
					if ($kk == $parsed_params['col_sku'] && empty($vv)) {
						$err = 1;
						$importlog->result .= "\n<br />".date('Y-m-d H:i:s'). " - у записи ".$key." отсутствуют данные о sku товара";
						$helper->saveImportlogActs($importlog);
					}
					if (isset($parsed_params['col_replace_points']) && in_array($kk, $parsed_params['col_replace_points'])) {
						$vv = str_replace(',','.',$vv);
					}
					if (isset($parsed_params['col_replace_space']) && in_array($kk, $parsed_params['col_replace_space'])) {
						$vv = str_replace(' ','',$vv);
					}
					if ($kk < $parsed_params['num_col'] && $err == 0) {
						$save_data[$key][$parsed_params['tab_names'][$kk]] = trim($vv);
					}
				};

				if($err == 0) {
					if (isset($save_data[$key]['quantity']) && $save_data[$key]['quantity'] != 0 && isset($save_data[$key]['weight'])) {
						$save_data[$key]['weight'] = $save_data[$key]['weight'] / $save_data[$key]['quantity'];
					} elseif(isset($save_data[$key]['weight'])) {
						//$save_data[$key]['weight'] = '0.000';
					}
					$save_data[$key]['created_at'] = date('Y-m-d H:i:s');
					$save_data[$key]['importlog_id'] = $importlog->id;
				}
			}
		};

		return $save_data;
    }

	/**
	 * Checks if importlog iexists (todel)
	*/
	public static function check_importlog_exists($id, $cond, $child)
    {
    	if (!empty($cond)) {
		    $importlog = Importlog::where([
	        	['id', '=', $id],
	        	$cond,
	        ])->first();
        } else {
        	$importlog = Importlog::find($id);
        }

		if (!$importlog) {
			echo redirect()->route('admin.importlogs.index')
				->with('error','Не найдено');
		}

		if($child && $importlog->$child->count() == 0) {
			echo redirect()->route('admin.importlogs.index')
				->with('error','Не найдено элементов в выгрузке');
		}

		return $importlog;
    }

	/**
	 * Creates antispam array
	 * @param array $sp_mass
	*/
	public static function antispam_check($sp_mass)
    {
        $sp_mass = array();
        
        $sp_mass[1]['calc'] = "0 nЛюC 5";
        $sp_mass[1]['result'] = "5";
        $sp_mass[2]['calc'] = "8 + дBА";
        $sp_mass[2]['result'] = "10";
        $sp_mass[3]['calc'] = "3 МИнУs 2";
        $sp_mass[3]['result'] = "1";
        $sp_mass[4]['calc'] = "1 + TРи";
        $sp_mass[4]['result'] = "4";
        $sp_mass[5]['calc'] = "ДBа + ПяtЬ";
        $sp_mass[5]['result'] = "7";
        $sp_mass[6]['calc'] = "1 + ОдИH";
        $sp_mass[6]['result'] = "2";
        $sp_mass[7]['calc'] = "ТpИ + ОдиН";
        $sp_mass[7]['result'] = "4";

        return $sp_mass;
    }

	/**
	 * Saves importlog and processes addit. actions if err
	 * @param object $importlog
	*/
	public static function saveImportlogActs($importlog)
    {
    	try {
			$importlog->save();
		} catch(\Illuminate\Database\QueryException $ex) {
			
		}
    }

}