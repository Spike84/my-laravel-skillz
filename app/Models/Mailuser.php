<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;


class Mailuser extends Model
{
    use HasFactory, Sortable;

    protected $fillable = [
        'fio',
        'email',
        'url',
        'user_id',
        'mailing_id',
        'is_sent'
    ];

	public $sortable = [
		'fio',
		'email',
		'url',
		'mailing_id',
		'is_sent'
	];

	public function mailing() {
        return $this->belongsTo(Mailing::class);
    }
}
