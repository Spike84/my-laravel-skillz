<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Importlog extends Model
{
    use HasFactory, Sortable;

    protected $fillable = [
        'user_id',
        'importtype_id',
        'importstage_id',
        'has_errors',
        'cat_id',
        'result',
        'started_at',
        'finished_at'
    ];

    public $sortable = [
		'type',
		'created_at',
		'updated_at',
		'started_at',
		'finished_at',
		'user.name',
		'importstage.name',
		'importtype.name',
	];

	public function importstage() {
        return $this->belongsTo(Importstage::class);
    }

    public function importtype() {
        return $this->belongsTo(Importtype::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

	public function cat() {
        return $this->belongsTo(Cat::class);
    }

    public function tproducts() {
        return $this->hasMany(Tproduct::class);
    }

	public function timgs() {
        return $this->hasMany(Timg::class);
    }

	public function tprops() {
        return $this->hasMany(Tprop::class);
    }
}
