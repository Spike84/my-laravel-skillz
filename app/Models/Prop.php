<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Prop extends Model
{
    use HasFactory, Sortable;

    protected $fillable = [
        'name',
        'sort',
    ];

	public $sortable = [
		'name',
		'sort',
	];

	public function propvals() {
        return $this->hasMany(Propval::class);
    }

}
