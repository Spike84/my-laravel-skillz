<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Timg extends Model
{
    use HasFactory, Sortable;

    protected $fillable = [
        'sku',
        'url',
        'file'
    ];

	public $sortable = [
		'sku',
	];

	public function product() {
        return $this->belongsTo(Product::class, 'sku', 'sku');
    }

	public function importlog() {
        return $this->belongsTo(Importlog::class);
    }

}
