<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Propval extends Model
{
    use HasFactory, Sortable;
    
    protected $fillable = [
        'prop_id',
        'product_id',
        'propvalue_id',
        'value',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

	public function prop()
    {
        return $this->belongsTo(Prop::class);
    }

	public function propvalue()
    {
        return $this->belongsTo(Propvalue::class);
    }

	public static function itemlist($viewdata) {
		//dump($viewdata);

		$items = array();

		$where[] = ['props.id', '!=', 0];

		if (isset($viewdata['cat_id'])) {
			$where[] = ['products.is_active', '=', 1];
			$where[] = ['products.cat_id', '=', $viewdata['cat_id']];

			if (isset($viewdata['in_stock']) && ($viewdata['in_stock'] == 1)) {
	    		$where[] = ['products.quantity', '>', 0];
	    	}
	    	if (isset($viewdata['searchw'])) {
	    		$cond[] = ['products.name', 'like', '%'.$viewdata['searchw'].'%'];
	    	}

			$items = self::select('propvals.propvalue_id', 'propvalues.value', 'propvals.prop_id', 'props.name', 'props.sort')
				->orderBy('props.sort','desc')
				->orderBy('propvalues.value','asc')
		        ->distinct()
		        ->leftJoin('props', 'propvals.prop_id', '=', 'props.id')
		    	->leftJoin('propvalues', 'propvals.propvalue_id', '=', 'propvalues.id');

			$items = $items->join('products', 'propvals.product_id', '=', 'products.id');
			$items = $items->where($where)
		        ->get()
	        	->groupBy('name');
    	}

        return $items;
    }

}
