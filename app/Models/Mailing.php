<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Mailing extends Model
{
    use HasFactory, Sortable;

    protected $fillable = [
        'name',
        'subject',
        'message',
        'log',
        'is_sent'
    ];

    public function mailusers() {
        return $this->hasMany(Mailuser::class);
    }

}
