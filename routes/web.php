<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\ProductsController;
use App\Http\Controllers\Admin\RolesController;
use App\Http\Controllers\Admin\RegionsController;
use App\Http\Controllers\Admin\PagesController;
use App\Http\Controllers\Admin\CatsController;
use App\Http\Controllers\Admin\PropsController;
use App\Http\Controllers\Admin\PropvalsController;
use App\Http\Controllers\Admin\TproductsController;
use App\Http\Controllers\Admin\ImportlogsController;
use App\Http\Controllers\Admin\ImgsController;
use App\Http\Controllers\Admin\TimgsController;
use App\Http\Controllers\Admin\TpropsController;
use App\Http\Controllers\Admin\OrdersController;
use App\Http\Controllers\Admin\MailingsController;
use App\Http\Controllers\Admin\MailusersController;
use App\Http\Controllers\Admin\SettingsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('layouts.app');
});*/

Route::get('/', 'App\Http\Controllers\PagesController@index');

Route::get('/products', 'App\Http\Controllers\ProductsController@index')->name('products');
Route::get('/products/{id}', 'App\Http\Controllers\ProductsController@show')->where('id', '[0-9]+')->name('products.show');
Route::get('/pages/{url}', 'App\Http\Controllers\PagesController@view')->where('url', '[A-Za-z0-9]+');
Route::get('/mailusers/index', 'App\Http\Controllers\MailusersController@index');
Route::get('/users/activate/{id}/{code}', 'App\Http\Controllers\UsersController@activate')->where('id', '[0-9]+')->where('code', '[0-9]+');
Route::get('/tproducts/create', 'App\Http\Controllers\TproductsController@create')->name('tproducts.create');

Route::group([
    'as' => 'mmail.',
    'prefix' => 'mmail',
], function () {
    Route::get('{url?}', 'App\Http\Controllers\MailusersController@edit')->where('url', '([A-z0-9-_\/.]+)?');
});

//public lk
Route::group([
    'as' => 'lk.',
    'prefix' => 'lk',
    'middleware' => ['auth']
], function () {
    Route::get('users/show', 'App\Http\Controllers\LK\UsersController@show')->name('users.show');
    Route::get('users/edit', 'App\Http\Controllers\LK\UsersController@edit')->name('users.edit');
    Route::post('users/update', 'App\Http\Controllers\LK\UsersController@update')->name('users.update');
    Route::post('baskets/ajax_add', 'App\Http\Controllers\LK\BasketsController@ajax_add')->name('baskets.ajax_add');
    Route::resource('baskets', App\Http\Controllers\LK\BasketsController::class);
    Route::resource('orders', App\Http\Controllers\LK\OrdersController::class, ['except' => ['index','create','edit']]);
});

Route::get('/admin', function () {
	if(!Auth::user()->hasPermissionTo('access-admin')) {
		return abort(404);
	}
    return view('layouts.admin');
})->middleware(['auth'])->name('dashboard');

//admin
Route::group([
    'as' => 'admin.',
    'prefix' => 'admin',
    'middleware' => ['auth']
], function () {
	Route::get('users/import', 'App\Http\Controllers\Admin\UsersController@import')->name('users.import');
	Route::get('orders/import', 'App\Http\Controllers\Admin\OrdersController@import')->name('orders.import');
	Route::get('timgs/del_files', 'App\Http\Controllers\Admin\TimgsController@del_files')->name('timgs.del_files');
	Route::get('imgs/del_imgs/{id}', 'App\Http\Controllers\Admin\ImgsController@del_imgs')->where('id', '[0-1]')->name('imgs.del_imgs');
	
    Route::resource('pages', PagesController::class, ['except' => ['show']]);
    Route::resource('products', ProductsController::class);
    Route::resource('roles', RolesController::class);
    Route::resource('users', App\Http\Controllers\Admin\UsersController::class);
    Route::resource('regions', RegionsController::class);
    Route::resource('cats', CatsController::class);
    Route::resource('props', PropsController::class);
	Route::resource('propvals', PropvalsController::class);
	Route::resource('tproducts', TproductsController::class, ['except' => ['index','create']]);
	Route::resource('tprops', TpropsController::class, ['except' => ['index','create','store','edit','update','show']]);

	Route::get('importlogs/clear_all', 'App\Http\Controllers\Admin\ImportlogsController@clear_all')->name('importlogs.clear_all');

	Route::resource('importlogs', ImportlogsController::class, ['except' => ['edit','update']]);
	Route::resource('imgs', ImgsController::class, ['except' => ['index']]);
	Route::resource('timgs', TimgsController::class, ['except' => ['index','create','edit','update']]);
	Route::resource('orders', OrdersController::class, ['except' => ['create','store']]);
	Route::resource('mailings', MailingsController::class);
	Route::resource('mailusers', MailusersController::class, ['except' => ['index','edit','update','show']]);
	Route::resource('settings', SettingsController::class, ['only' => ['index']]);

	Route::get('pages/show', 'App\Http\Controllers\Admin\PagesController@show')->name('pages.show');
	Route::post('products/save_img', 'App\Http\Controllers\Admin\ProductsController@save_image')->name('products.save_img');
	Route::get('users/activate/{id}/{code}', 'App\Http\Controllers\Admin\UsersController@activate')->where('id', '[0-9]+')->where('code', '[0-9]+')->name('users.activate');
	Route::get('tproducts/index/{id}', 'App\Http\Controllers\Admin\TproductsController@index')->where('id', '[0-9]+')->name('tproducts.index');
	Route::get('tproducts/create/{id}', 'App\Http\Controllers\Admin\TproductsController@create')->where('id', '[0-9]+')->name('tproducts.create');
	Route::get('timgs/index/{id}', 'App\Http\Controllers\Admin\TimgsController@index')->where('id', '[0-9]+')->name('timgs.index');
	Route::get('timgs/create/{id}', 'App\Http\Controllers\Admin\TimgsController@create')->where('id', '[0-9]+')->name('timgs.create');
	Route::get('timgs/edit/{id}', 'App\Http\Controllers\Admin\TimgsController@edit')->where('id', '[0-9]+')->name('timgs.edit');
	Route::get('timgs/update/{id}', 'App\Http\Controllers\Admin\TimgsController@update')->where('id', '[0-9]+')->name('timgs.update');
	Route::get('tprops/index/{id}', 'App\Http\Controllers\Admin\TpropsController@index')->where('id', '[0-9]+')->name('tprops.index');
	Route::get('tprops/create/{id}', 'App\Http\Controllers\Admin\TpropsController@create')->where('id', '[0-9]+')->name('tprops.create');
	Route::get('tprops/edit/{id}', 'App\Http\Controllers\Admin\TpropsController@edit')->where('id', '[0-9]+')->name('tprops.edit');
	Route::get('tprops/update/{id}', 'App\Http\Controllers\Admin\TpropsController@update')->where('id', '[0-9]+')->name('tprops.update');
	Route::get('tproducts/sync/{id}', 'App\Http\Controllers\Admin\TproductsController@sync')->where('id', '[0-9]+')->name('tproducts.sync');
	Route::get('mailings/build/{id}', 'App\Http\Controllers\Admin\MailingsController@build')->where('id', '[0-9]+')->name('mailings.build');
	Route::get('mailings/arrange/{id}', 'App\Http\Controllers\Admin\MailingsController@arrange')->where('id', '[0-9]+')->name('mailings.arrange');
	Route::get('mailusers/index/{id}', 'App\Http\Controllers\Admin\MailusersController@index')->where('id', '[0-9]+')->name('mailusers.index');
	Route::get('settings/edit/{id}/{is_active}', 'App\Http\Controllers\Admin\SettingsController@edit')->where('id', '[0-9]+')->where('is_active', '[0-1]+')->name('settings.edit');
    //Route::get('users', 'App\Http\Controllers\Admin\UsersController@index');
    
});


require __DIR__.'/auth.php';

/*
DB::listen(function($query) {
    echo "<pre>";
    var_dump($query->sql, $query->bindings, $query->time);
    echo "</pre>";
});
*/